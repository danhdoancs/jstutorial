'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Gomoku = mongoose.model('Gomoku'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, gomoku;

/**
 * Gomoku routes tests
 */
describe('Gomoku CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Gomoku
		user.save(function() {
			gomoku = {
				name: 'Gomoku Name'
			};

			done();
		});
	});

	it('should be able to save Gomoku instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Gomoku
				agent.post('/gomokus')
					.send(gomoku)
					.expect(200)
					.end(function(gomokuSaveErr, gomokuSaveRes) {
						// Handle Gomoku save error
						if (gomokuSaveErr) done(gomokuSaveErr);

						// Get a list of Gomokus
						agent.get('/gomokus')
							.end(function(gomokusGetErr, gomokusGetRes) {
								// Handle Gomoku save error
								if (gomokusGetErr) done(gomokusGetErr);

								// Get Gomokus list
								var gomokus = gomokusGetRes.body;

								// Set assertions
								(gomokus[0].user._id).should.equal(userId);
								(gomokus[0].name).should.match('Gomoku Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Gomoku instance if not logged in', function(done) {
		agent.post('/gomokus')
			.send(gomoku)
			.expect(401)
			.end(function(gomokuSaveErr, gomokuSaveRes) {
				// Call the assertion callback
				done(gomokuSaveErr);
			});
	});

	it('should not be able to save Gomoku instance if no name is provided', function(done) {
		// Invalidate name field
		gomoku.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Gomoku
				agent.post('/gomokus')
					.send(gomoku)
					.expect(400)
					.end(function(gomokuSaveErr, gomokuSaveRes) {
						// Set message assertion
						(gomokuSaveRes.body.message).should.match('Please fill Gomoku name');
						
						// Handle Gomoku save error
						done(gomokuSaveErr);
					});
			});
	});

	it('should be able to update Gomoku instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Gomoku
				agent.post('/gomokus')
					.send(gomoku)
					.expect(200)
					.end(function(gomokuSaveErr, gomokuSaveRes) {
						// Handle Gomoku save error
						if (gomokuSaveErr) done(gomokuSaveErr);

						// Update Gomoku name
						gomoku.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Gomoku
						agent.put('/gomokus/' + gomokuSaveRes.body._id)
							.send(gomoku)
							.expect(200)
							.end(function(gomokuUpdateErr, gomokuUpdateRes) {
								// Handle Gomoku update error
								if (gomokuUpdateErr) done(gomokuUpdateErr);

								// Set assertions
								(gomokuUpdateRes.body._id).should.equal(gomokuSaveRes.body._id);
								(gomokuUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Gomokus if not signed in', function(done) {
		// Create new Gomoku model instance
		var gomokuObj = new Gomoku(gomoku);

		// Save the Gomoku
		gomokuObj.save(function() {
			// Request Gomokus
			request(app).get('/gomokus')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Gomoku if not signed in', function(done) {
		// Create new Gomoku model instance
		var gomokuObj = new Gomoku(gomoku);

		// Save the Gomoku
		gomokuObj.save(function() {
			request(app).get('/gomokus/' + gomokuObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', gomoku.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Gomoku instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Gomoku
				agent.post('/gomokus')
					.send(gomoku)
					.expect(200)
					.end(function(gomokuSaveErr, gomokuSaveRes) {
						// Handle Gomoku save error
						if (gomokuSaveErr) done(gomokuSaveErr);

						// Delete existing Gomoku
						agent.delete('/gomokus/' + gomokuSaveRes.body._id)
							.send(gomoku)
							.expect(200)
							.end(function(gomokuDeleteErr, gomokuDeleteRes) {
								// Handle Gomoku error error
								if (gomokuDeleteErr) done(gomokuDeleteErr);

								// Set assertions
								(gomokuDeleteRes.body._id).should.equal(gomokuSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Gomoku instance if not signed in', function(done) {
		// Set Gomoku user 
		gomoku.user = user;

		// Create new Gomoku model instance
		var gomokuObj = new Gomoku(gomoku);

		// Save the Gomoku
		gomokuObj.save(function() {
			// Try deleting Gomoku
			request(app).delete('/gomokus/' + gomokuObj._id)
			.expect(401)
			.end(function(gomokuDeleteErr, gomokuDeleteRes) {
				// Set message assertion
				(gomokuDeleteRes.body.message).should.match('User is not logged in');

				// Handle Gomoku error error
				done(gomokuDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Gomoku.remove().exec();
		done();
	});
});