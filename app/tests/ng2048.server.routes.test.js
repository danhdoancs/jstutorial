'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Ng2048 = mongoose.model('Ng2048'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, ng2048;

/**
 * Ng2048 routes tests
 */
describe('Ng2048 CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Ng2048
		user.save(function() {
			ng2048 = {
				name: 'Ng2048 Name'
			};

			done();
		});
	});

	it('should be able to save Ng2048 instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Ng2048
				agent.post('/ng2048s')
					.send(ng2048)
					.expect(200)
					.end(function(ng2048SaveErr, ng2048SaveRes) {
						// Handle Ng2048 save error
						if (ng2048SaveErr) done(ng2048SaveErr);

						// Get a list of Ng2048s
						agent.get('/ng2048s')
							.end(function(ng2048sGetErr, ng2048sGetRes) {
								// Handle Ng2048 save error
								if (ng2048sGetErr) done(ng2048sGetErr);

								// Get Ng2048s list
								var ng2048s = ng2048sGetRes.body;

								// Set assertions
								(ng2048s[0].user._id).should.equal(userId);
								(ng2048s[0].name).should.match('Ng2048 Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Ng2048 instance if not logged in', function(done) {
		agent.post('/ng2048s')
			.send(ng2048)
			.expect(401)
			.end(function(ng2048SaveErr, ng2048SaveRes) {
				// Call the assertion callback
				done(ng2048SaveErr);
			});
	});

	it('should not be able to save Ng2048 instance if no name is provided', function(done) {
		// Invalidate name field
		ng2048.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Ng2048
				agent.post('/ng2048s')
					.send(ng2048)
					.expect(400)
					.end(function(ng2048SaveErr, ng2048SaveRes) {
						// Set message assertion
						(ng2048SaveRes.body.message).should.match('Please fill Ng2048 name');
						
						// Handle Ng2048 save error
						done(ng2048SaveErr);
					});
			});
	});

	it('should be able to update Ng2048 instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Ng2048
				agent.post('/ng2048s')
					.send(ng2048)
					.expect(200)
					.end(function(ng2048SaveErr, ng2048SaveRes) {
						// Handle Ng2048 save error
						if (ng2048SaveErr) done(ng2048SaveErr);

						// Update Ng2048 name
						ng2048.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Ng2048
						agent.put('/ng2048s/' + ng2048SaveRes.body._id)
							.send(ng2048)
							.expect(200)
							.end(function(ng2048UpdateErr, ng2048UpdateRes) {
								// Handle Ng2048 update error
								if (ng2048UpdateErr) done(ng2048UpdateErr);

								// Set assertions
								(ng2048UpdateRes.body._id).should.equal(ng2048SaveRes.body._id);
								(ng2048UpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Ng2048s if not signed in', function(done) {
		// Create new Ng2048 model instance
		var ng2048Obj = new Ng2048(ng2048);

		// Save the Ng2048
		ng2048Obj.save(function() {
			// Request Ng2048s
			request(app).get('/ng2048s')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Ng2048 if not signed in', function(done) {
		// Create new Ng2048 model instance
		var ng2048Obj = new Ng2048(ng2048);

		// Save the Ng2048
		ng2048Obj.save(function() {
			request(app).get('/ng2048s/' + ng2048Obj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', ng2048.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Ng2048 instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Ng2048
				agent.post('/ng2048s')
					.send(ng2048)
					.expect(200)
					.end(function(ng2048SaveErr, ng2048SaveRes) {
						// Handle Ng2048 save error
						if (ng2048SaveErr) done(ng2048SaveErr);

						// Delete existing Ng2048
						agent.delete('/ng2048s/' + ng2048SaveRes.body._id)
							.send(ng2048)
							.expect(200)
							.end(function(ng2048DeleteErr, ng2048DeleteRes) {
								// Handle Ng2048 error error
								if (ng2048DeleteErr) done(ng2048DeleteErr);

								// Set assertions
								(ng2048DeleteRes.body._id).should.equal(ng2048SaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Ng2048 instance if not signed in', function(done) {
		// Set Ng2048 user 
		ng2048.user = user;

		// Create new Ng2048 model instance
		var ng2048Obj = new Ng2048(ng2048);

		// Save the Ng2048
		ng2048Obj.save(function() {
			// Try deleting Ng2048
			request(app).delete('/ng2048s/' + ng2048Obj._id)
			.expect(401)
			.end(function(ng2048DeleteErr, ng2048DeleteRes) {
				// Set message assertion
				(ng2048DeleteRes.body.message).should.match('User is not logged in');

				// Handle Ng2048 error error
				done(ng2048DeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Ng2048.remove().exec();
		done();
	});
});