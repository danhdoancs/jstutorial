'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
        Schema = mongoose.Schema;

/**
 * Ng2048 Schema
 */
var Ng2048Schema = new Schema({
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        index: true,
        require: true
    },
    fbId: {
        type: String,
        index: true,
        require: true
    },
    highScore: {
        type: Number,
        default: 0,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('Ng2048', Ng2048Schema);