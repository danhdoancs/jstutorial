'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Gomoku Schema
 */
var GomokuSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Gomoku name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Gomoku', GomokuSchema);