'use strict';

module.exports = function (app) {
    var users = require('../../app/controllers/users.server.controller');
    var ng2048s = require('../../app/controllers/ng2048s.server.controller');

    // Ng2048s Routes
    app.route('/2048')
            .get(ng2048s.ng2048ByUser, ng2048s.read)
            .post(users.requiresLogin, ng2048s.ng2048ByUser, ng2048s.create)
            .put(users.requiresLogin, ng2048s.ng2048ByUser, ng2048s.update);

    app.route('/2048/leaderboard/:category')
            .get(ng2048s.listToppers)
            .post(users.requiresLogin, ng2048s.listTopFriends);

//            app.route('/2048/session').get(function(req, res) {
//                return res.send(req.user);
//            });
    // Finish by binding the Ng2048 middleware
//    app.param('2048Id', ng2048s.ng2048ByID);
};
