'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var gomokus = require('../../app/controllers/gomokus.server.controller');

	// Gomokus Routes
//        app.route('/gomokusocket')
//                .get(gomokus.setupSocket);
	app.route('/gomoku')
		.get(gomokus.list)
		.post(users.requiresLogin, gomokus.create);

	app.route('/gomoku/:gomokuId')
		.get(gomokus.read)
		.put(users.requiresLogin, gomokus.hasAuthorization, gomokus.update)
		.delete(users.requiresLogin, gomokus.hasAuthorization, gomokus.delete);

	// Finish by binding the Gomoku middleware
	app.param('gomokuId', gomokus.gomokuByID);
};
