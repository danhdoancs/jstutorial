'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
        errorHandler = require('../errors.server.controller'),
        mongoose = require('mongoose'),
//        passport = require('passport'),
        User = mongoose.model('User'),
        users = require('../users.server.controller');

exports.friendlist = function (req, res) {
    User.find().where('username').in(req.body.ids).limit(500)
            .select('status socketId username displayName providerData.link')
            .sort('-status firstName').exec(function (err, friends) {
//        if (err) {
//            return res.jsonp([{
//                    error: err.toString() + '\n' + errorHandler.getErrorMessage(err)
//                }]);
//        } else {
//            res.jsonp(ng2048s);
//        }
        if (!err) {
            res.jsonp(friends);
        } else {
            res.send(null);
        }
    });
};

exports.facebookLogin = function (req, res) {
    //check db if user exists
    var profile = req.body.user;
    delete profile.picture;

    User.findOne({username: profile.id}, function (err, user) {
        // Error occured
        if (err) {
            return res.status(400).send({status: -1, message: err});
        }
        // No error, new user
        if (!user) {
            // User not exists, create one
            // Set the provider data and include tokens
//            profile.accessToken = req.body.authRes.accessToken;

            // Save the user OAuth profile
            user = new User({
                status: 1,
                socketId: profile.socketId,
                firstName: profile.first_name,
                lastName: profile.last_name,
                username: profile.id,
                displayName: profile.name,
                email: profile.email,
                provider: 'facebook',
                providerData: profile
            });

            // And save the user
            user.save(function (err) {
                if (err)
                    return res.status(400).send({status: -2, message: 'Cannot create new user'});
                else {
                    //Created successful, save session
                    req.session.user = user._id;
                    //Return message
                    return res.send({status: 1, message: 'Created new user, logged in'});

                }
            });
        } else {
            //set online status
            user.status = 1;
            user.socketId = profile.socketId;
            // Check for update
            if (profile.updated_time > user.providerData.updated_time) {
                // Save the user OAuth profile
                user.firstName = profile.first_name;
                user.lastName = profile.last_name;
                user.displayName = profile.name;
                user.providerData = profile;
            }

            // And save the user
            user.save(function (err) {
                if (err)
                    return res.status(400).send({status: -2, message: 'Cannot update user'});
                else {
                    //Created successful, save session
                    req.session.user = user._id;
                    //Return message
                    return res.send({status: 1, message: 'Updated user, logged in'});

                }
            });
        }
    });
    //save session
};

exports.facebookLogout = function (req, res) {
    //logout of system
    console.log(req.session.user);
    User.update({_id: req.session.user}, {status: 0}, function (err) {
        if (err)
            res.status(400).send({status: -2, message: 'Cannot update user'});
        else {
            res.send({status: 1, message: 'Logout successful'});
        }

        //delete session
        delete req.session.user;
    });
};
