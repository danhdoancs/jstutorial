'use strict';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var
        GameServer = module.exports = {games: {}, game_count: 0},
UUID = require('node-uuid'),
        verbose = true;

//Import shared game library code.
var GameCore = require('./game.server.controller.js');

//A simple wrapper for logging so we can toggle it,
//and augment it for clarity.
GameServer.log = function () {
    if (verbose)
        console.log.apply(this, arguments);
};

GameServer.onMessage = function (client, message) {
    //Cut the message up into sub components
    var message_parts = message.split('.');
    //The first is always the type of message
    var message_type = message_parts[0];
    // Message data
    var message_data = message_parts[1] || null;

//    var other_client =
//            (client.game.player_host.id === client.id) ?
//            client.game.player_client : client.game.player_host;

    //challenge the world request
    if (message_type === 'ch') {
        this.onChallenge(client, message_data);
    } else if (message_type === 'lgin') { //user logged in, update facebook id
        var msg_datas = message_data.split('#');
        client.fbid = msg_datas[0];
        client.fbtoken = msg_datas[1];
//        this.log(client);
    } else if (message_type === 'more') { //player request play another game
        client.requestAnotherGame = true;
        client.isWinner = false;
        //response restart game if receive both requests
        if (client.hosting && client.game.player_client.requestAnotherGame) {
            client.send('rs.');
            client.game.player_client.send('rs.');
            //clear flag
            client.requestAnotherGame = false;
            client.game.player_client.requestAnotherGame = false;
            console.log('response restart new game');
        } else if (!client.hosting && client.game.player_host.requestAnotherGame) {
            //set flag
            client.send('rs.');
            client.game.player_host.send('rs.');
            //clear flag
            client.requestAnotherGame = false;
            client.game.player_host.requestAnotherGame = false;
            console.log('response restart new game');
        }
    } else if (message_type === 'finish') {
        //save result to database
        //delete the game
        this.onFinishGame(client);
    }
    else if (message_type === 'p') { // server request ping
        client.send('p.' + message_parts[1]);
    }
    ;

}; //game_server.onMessage

GameServer.onMove = function (client, parts) {

}; //game_server.onInput

GameServer.onChallenge = function (client, flag) {
    if (flag === '1') {
        //now we can find them a game to play with someone.
        //if no game exists with someone waiting, they create one and wait.
        this.findGame(client);
    } else {
        this.cancleFindGame(client);
    }
};

GameServer.onPlayWithFriend = function (client, guest, flag) {
    if (flag === 1) {
        var game = this.createGame(client);
        game.player_client = guest;
//                game.gamecore.players.other.instance = player;
        game.player_count++;

        //start running the game on the server,
        //which will tell them to respawn/start
        this.startGame(game);
        console.log('set up the game for friends');
    }
}

//Check if player is in the game or not
GameServer.isPlayerInAnyGame = function (player) {
    var games = this.games;
    for (var i = 0; i < games.length; i++) {
        if (games[i].player_host.id === player.oldId) {
            games[i].player_host = player;
            player.game = games[i];
            player.hosting = true;
            this.log('reconnected to the game');
            return true;
        }
        if (games[i].player_client && games[i].player_client.id === player.oldId) {
            games[i].player_client = player;
            player.game = games[i];
            this.log('reconnected to the game');
            return true;
        }
    }
    this.log('reconnected, but no game found');
    return false;
};

//Define some required functions
GameServer.createGame = function (player) {

    //Create a new game instance
    var thegame = {
        id: UUID(), //generate a new id for the game
        player_host: player, //so we know who initiated the game
        player_client: null, //nobody else joined yet, since its new
        player_count: 1              //for simple checking of state
    };

    //Store it in the list of game
    this.games[ thegame.id ] = thegame;

    //Keep track
    this.game_count++;

    //Create a new game core instance, this actually runs the
    //game code like collisions and such.
//    thegame.gamecore = new GameCore(thegame);

    //tell the player that they are now the host
    //s=server message, h=you are hosting

    player.send('h.');
    player.game = thegame;
    player.hosting = true;

    this.log('player ' + player.id + ' created a game with id ' + player.game.id);

    //return it
    return thegame;

}; //game_server.createGame

//we are requesting to kill a game in progress.
GameServer.onFinishGame = function (player) {
    var thegame = this.games[player.game.id];
    // if the game is still exist, then player is the first one request to finish
    // send finish response for the other player and delete the game
    if (thegame) {
        //send the players the message the game is ending
        if (player === thegame.player_host) {
            thegame.player_client.send('e.');
        } else {
            thegame.player_host.send('e.');
        }
        delete this.games[thegame.id];
        this.game_count--;
        this.log('game finished. there are now ' + this.game_count + ' games');
    } else {
        this.log('other player already quit, game was deleted!');
    }
}; //game_server.endGame

//we are requesting to kill a game in progress.
GameServer.endGame = function (gameid, userid) {
    var thegame = this.games[gameid];
    if (thegame) {
        //if the game has two players, the one is leaving
        if (thegame.player_count > 1) {
            //send the players the message the game is ending
            if (userid === thegame.player_host.id) {
                //the host left, oh snap. Lets try join another game
                if (thegame.player_client) {
                    //tell them the game is over
                    thegame.player_client.send('e');
                    //now look for/create a new game.
//                        this.findGame(thegame.player_client);
                }
            } else {
                //the other player left, we were hosting
                if (thegame.player_host) {
                    //tell the client the game is ended
                    thegame.player_host.send('e');
                    //i am no longer hosting, this game is going down
                    thegame.player_host.hosting = false;
                    //now look for/create a new game.
//                        this.findGame(thegame.player_host);
                }
            }
        }

        delete this.games[gameid];
        this.game_count--;
        this.log('game removed. there are now ' + this.game_count + ' games');
    } else {
        this.log('that game was not found!');
    }

}; //game_server.endGame

GameServer.startGame = function (game) {

    //right so a game has 2 players and wants to begin
    //the host already knows they are hosting,
    //tell the other client they are joining a game
    //s=server message, j=you are joining, send them the host id
    game.player_client.send('j.' + game.player_host.id);
    game.player_client.game = game;
    //send facebook ids
    game.player_client.send('r.' + game.player_host.fbid + "#" + game.player_host.fbtoken);
    game.player_host.send('r.' + game.player_client.fbid + "#" + game.player_client.fbtoken);

    //set this flag, so that the update loop can run it.
    game.active = true;

    this.log('game started');

}; //game_server.startGame

GameServer.findGame = function (player) {

    //check user logged in
    if (!player.fbid)
    {
        this.log(player.id + 'not logged in yet, stop finding game');
        //anounce client
        player.send('unauthorized.');
        return false;
    }

    this.log('looking for a game. We have : ' + this.game_count);

    //check if player already in a game, so delete that game first
    if (player.game) {
        //delete the old game first
        this.log('player already in a game. cancling old game...');
        this.endGame(player.game.id, player.id);
//        this.onFinishGame(player);
    }

    //so there are games active,
    //lets see if one needs another player
    if (this.game_count) {
        var joined_a_game = false;
        var game;
        //Check the list of games for an open game
        for (var gameid in this.games) {
            //stop finding game if player cancle
            if (player.flagStopFindGame) {
                this.log('Stop finding game');
                player.flagStopFindGame = false;
                return false;
            }
            //only care about our own properties.
            if (!this.games.hasOwnProperty(gameid))
                continue;

            game = this.games[gameid];
            //If the game is a player short
            if (game.player_count < 2) {
                //check if user already waiting in that game
                if (game.player_host.fbid === player.fbid) {
                    this.log('host fbid: ' + game.player_host.fbid + ', player fbid: ' + player.fbid);
                    this.log('player already hosting a game, skip: ' + player.id);
                    continue;
                }
                this.log(player.id + 'found game, join');
//                player.send('j.');
                //someone wants us to join!
                joined_a_game = true;
                //increase the player count and store
                //the player as the client of this game
                game.player_client = player;
//                game.gamecore.players.other.instance = player;
                game.player_count++;

                //start running the game on the server,
                //which will tell them to respawn/start
                this.startGame(game);

            } //if less than 2 players
        } //for all games

        //now if we didn't join a game,
        //we must create one
        if (!joined_a_game) {

            this.createGame(player);
            this.log('no games need player, created one');
        } //if no join already

    } else { //if there are any games at all
        //no games? create one!
        this.createGame(player);
        this.log('no games, created one');
    }

}; //game_server.findGame

GameServer.cancleFindGame = function (player) {
//    this.log('cancle finding game');
    var game = player.game;
    if (game) {
        //if player hosting a game
        if (game.player_host === player) {
            //if the game already has guest
            if (game.player_client) {
                //switch the guest to host the game
                game.player_host = game.player_client;
                game.player_client = null;
                player.game.player_count--;
                // find another player for the client
                this.findGame(game.player_host);
            } else {
                //delete the empty game
                delete this.games[game.id];
                delete player.game;
                this.game_count--;
            }
            this.log(player.id + ' cancled hosting a game');
            //is client of a game
        } else if (game.player_client === player) {
            player.game = null;
            game.player_count--;
            //find game again for the host
            this.findGame(game.player_host);
            this.log(player.id + 'droped out a game');
        }
    } else {
        //set flag not finding game for this player
        player.flagStopFindGame = true;
        this.log(player.id + ' cancled finding game');
    }
};