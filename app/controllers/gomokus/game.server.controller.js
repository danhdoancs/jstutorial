'use strict';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Now the main game class. This gets created on
//both server and client. Server creates one for
//each game that is hosted, and client creates one
//for itself to play the game.

var Player = require('./player.server.controller.js');

/* The game_core class */
function GameCore(game_instance) {

    //Store the instance, if any
    this.instance = game_instance;

    //We create a player set, passing them
    //the game that is running them, as well
    this.players = {
        self: new Player(this, this.instance.player_host),
        other: new Player(this, this.instance.player_client)
    };
}

//Makes sure things run smoothly and notifies clients of changes
//on the server side
GameCore.prototype.update = function () {

    //Make a snapshot of the current state, for updating the clients
    this.laststate = {
        hp: this.players.self.pos, //'host position', the game creators position
        cp: this.players.other.pos, //'client position', the person that joined, their position
    };

    //Send the snapshot to the 'host' player
    if (this.players.self.instance) {
        this.players.self.instance.emit('onserverupdate', this.laststate);
    }

    //Send the snapshot to the 'client' player
    if (this.players.other.instance) {
        this.players.other.instance.emit('onserverupdate', this.laststate);
    }

}; //game_core.server_update

module.exports = GameCore;