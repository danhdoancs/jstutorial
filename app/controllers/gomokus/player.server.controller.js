'use strict';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 The player class
 A simple class to maintain state of a player on screen,
 as well as to draw that state when required.
 */

function Player(game_instance, player_instance) {
    //Store the instance, if any
    this.instance = player_instance;
    this.game = game_instance;

    //Set up initial values for our state information
    this.state = 'not-connected';
    this.id = '';
    this.fbid = '';
    this.isOnline = true;
    this.hosting = false;

    //These are used in moving us around later
//        this.old_state = {pos:{x:0,y:0}};
//        this.cur_state = {pos:{x:0,y:0}};

} //game_player.constructor

module.exports = Player;