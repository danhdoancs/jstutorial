'use strict';

/**
 * Module dependencies.
 */
var GameServer = require('./gomokus/coordinator.server.controller');


exports.index = function (req, res) {

    res.render('index', {
        user: req.user || null,
        request: req
    });
};

exports.socket = function (req, res) {
    var socketio = req.app.get('socketio');
    var invitedClientList = Object.create(null);

    socketio.on('connection', function (client) {
        //check if client already connected
        if (client.registered)
        {
            console.log('player already connected: ' + client.id);
            return false;
        }
        client.registered = true;

        //Useful to know when someone connects
        console.log('\t socket.io:: player ' + client.id + ' connected');

        //save socket id in user database


//        console.log(client);
//        flagConnected = true;
//        client.send(client.id);
        //tell the player they connected, giving them their id
        client.emit('onconnected', client.id);

        //Now we want to handle some of the messages that clients will send.
        //They send messages here, and we send them to the game_server to handle.
        client.on('message', function (msg) {
            GameServer.onMessage(client, msg);
        }); //client.on message

        //When client send invitation
        client.on('invite', function (msg) {
            var msgs = msg.split('.');
            if (msgs[0] === '0') { //invite 
                socketio.to(msgs[2]).emit('message', 'inv.' + msgs[1] + '.' + client.id);
                console.log('sent invitation to ' + msg);
            } else if (msgs[0] === '1' || msgs[0] === '-1') { //response from guest
                //save invited client to list
                invitedClientList[client.id] = client;
//                socketio.to(msgs[]);
                socketio.to(msgs[2]).emit('message', 'repinv.' + msgs[0] + '.' + msgs[1] + '.' + client.id);
                console.log('reply invitation to ' + msg);
            } else if (msgs[0] === '2') { //confirm to play from invitor
                console.log('set up game for 2 friends');
//                console.log(socketio.sockets.sockets);
//                console.log(invitedClientList[msgs[2]]);
                GameServer.onPlayWithFriend(client, invitedClientList[msgs[2]], 1);
                delete invitedClientList[msgs[2]];
            } else { //cancle the invitation
                socketio.to(msgs[2]).emit('message', 'cfminv.' + msgs[0] + '.' + msgs[1] + '.' + client.id);
                console.log('invitor reply decision to ' + msg);
                delete invitedClientList[msgs[2]];
            }
        });

        //when this client play a chess
        client.on('move', function (move) {
            console.log('move');
            //check if the game is deleted
            if (!client.game) {
                console.log('the game is deleted, cannot move');
                return false;
            }

            console.log('player ' + client.id + ' move: ' + move.pos);
            console.log('host id: ' + client.game.player_host.id);
            console.log('client id: ' + client.game.player_client.id);
            //emit for other player
            if (move.from === 'host')
                socketio.to(client.game.player_client.id).emit('move', move.pos);
            else
                socketio.to(client.game.player_host.id).emit('move', move.pos);
        });

        client.on('end', function (data) {
            data = parseInt(data);
            client.isWinner = data;
            if (data === 1)
                client.totalWons++;
            //log
            console.log('game ended: ' + data);
        });

        //Player reconnect, send id to reconnect, enter old game
        client.on('reconnect', function (clientOldId) {
            if (!clientOldId)
                return false;
            //update client info
            client.oldId = clientOldId;
            //game server check if player is in anygame
            //if so, replace new client
            if (GameServer.isPlayerInAnyGame(client)) {
                flagConnected = true;
                console.log(client.id + 'found old game, reconnected');
            }
        });

        //When this client disconnects
        client.on('disconnect', function () {
            client.isOnline = false;
            //will disconnect player after 10s if not reconnect
            setTimeout(function () {
                //return if player reconnected
                if (client.isOnline) {
                    console.log('client reconnected successful');
                } else {
                    //Useful to know when someone disconnects
                    console.log('\t socket.io:: client disconnected ' + client.id);

                    if (client.game && client.game.id) {
                        //player leaving a game should destroy that game
                        GameServer.endGame(client.game.id, client.id);
                    } //client.game_id
                }
                //delete the old client instance
                client.disconnect();
            }, 10000);
        }); //client.on disconnect
    });
    res.send('connected socket server');
};