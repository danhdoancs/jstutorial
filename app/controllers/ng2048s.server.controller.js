'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
        errorHandler = require('./errors.server.controller'),
        Ng2048 = mongoose.model('Ng2048'),
        _ = require('lodash'),
        AES = require('crypto-js/aes'),
        ENC = require('crypto-js/enc-utf8');
/**
 * Create a Ng2048
 */
exports.create = function (req, res) {
    if (req.ng2048)
        return res.send({message: 'game exists', status: 0});

    //decode
    var highScore = ENC.stringify(AES.decrypt(req.body.highScore, 'you'));

    if (!highScore || highScore === '')
        return res.send({
            error: 'unknown error'
        });
    else
        req.body.highScore = highScore;

    var ng2048 = new Ng2048(req.body);
    // Add user
    ng2048.user = req.user;

    ng2048.save(function (err) {
        if (!err) {
            return res.jsonp(ng2048);
        } else {
            return res.send({
                error: err.toString() + errorHandler.getErrorMessage(err)
            });
        }
    });
};

/**
 * Show the current Ng2048
 */
exports.read = function (req, res) {
    res.jsonp(req.ng2048);
};

/**
 * Update a Ng2048
 */
exports.update = function (req, res) {
    var ng2048 = req.ng2048;

    //decode
    var highScore = ENC.stringify(AES.decrypt(req.body.highScore, 'you'));

    if (!highScore || highScore === '')
        return res.send({
            error: 'unknown error'
        });
    else
        req.body.highScore = highScore;

    ng2048 = _.extend(ng2048, req.body);

    ng2048.save(function (err) {
        if (!err) {
            return res.send(ng2048);
        } else {
            return res.send({
                error: err.toString() + '\n' + errorHandler.getErrorMessage(err)
            });
        }
    });
};

/**
 * Delete an Ng2048
 */
//exports.delete = function(req, res) {
//	var ng2048 = req.ng2048 ;
//
//	ng2048.remove(function(err) {
//		if (err) {
//			return res.status(400).send({
//				message: errorHandler.getErrorMessage(err)
//			});
//		} else {
//			res.jsonp(ng2048);
//		}
//	});
//};

/**
 * List of Ng2048s
 */
exports.listToppers = function (req, res) {
    Ng2048.find().sort('-highScore').limit(100).select('highScore user').populate('user', 'displayName providerData.id providerData.link').exec(function (err, ng2048s) {
        if (!err) {
            res.jsonp(ng2048s);
        } else
            res.send(null);
    });
};
exports.listTopFriends = function (req, res) {
//      res.jsonp({status:1, data:req.body.friends});return;
    Ng2048.find().where('fbId').in(req.body.friends).sort('-highScore').limit(500).select('highScore user').populate('user', 'displayName providerData.id providerData.link').exec(function (err, ng2048s) {
//        if (err) {
//            return res.jsonp([{
//                    error: err.toString() + '\n' + errorHandler.getErrorMessage(err)
//                }]);
//        } else {
//            res.jsonp(ng2048s);
//        }
        if (!err) {
            res.jsonp(ng2048s);
        } else {
            res.send(null);
        }
    });
};

/**
 * Ng2048 middleware
 */
//exports.ng2048ByID = function (req, res, next, id) {
//    Ng2048.findById(id).populate('user', 'displayName').exec(function (err, ng2048) {
//        if (err)
//            return next(err);
//        if (!ng2048)
//            return next(new Error('Failed to load Ng2048 ' + id));
//        req.ng2048 = ng2048;
//        next();
//    });
//};
exports.ng2048ByUser = function (req, res, next) {
    if (!req.user)
        return res.send({message: 'User not found'});

    Ng2048.findOne({'user': req.user}).select('user highScore').populate('user', 'id').exec(function (err, ng2048) {
        if (err)
            return next(err);
//        if (!ng2048)
//            return next(new Error('Failed to load 2048 of ' + req.user));
        req.ng2048 = ng2048;
        next();
    });
};

/**
 * Ng2048 authorization middleware
 */
exports.hasAuthorization = function (req, res, next) {
    if (req.ng2048.user.id !== req.user) {
        return res.status(403).send('User is not authorized');
    }
    next();
};
