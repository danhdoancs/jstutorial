'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
require('../app/models/user.server.model');
var User = mongoose.model('User');

/**
 * Module init function.
 */
module.exports = function () {
    return function(req, res, next) {

        var user = req.session.user;

        if (user)
        {
                req.user = user;
        }

        next();
    };
};
//module.exports = function () {
//    return function(req, res, next) {
//        var suser = req.session.user;
//        if (suser)
//        {
//            User.findOne({
//                _id: suser
//                }, function (err, user) {
//            if (!err)
//                user.id = user._id;
//                req.user = user;
//            });
//        }
//        next();
//    };
//};