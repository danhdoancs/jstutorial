'use strict';

module.exports = {
    app: {
        title: 'CloudyBoard',
        description: 'Board games on the cloud',
        keywords: 'board games, cloud, browser games, 2048, domino, sudoku, chess'
    },
    port: process.env.PORT || 3000,
    templateEngine: 'swig',
    sessionSecret: 'CLOUDYBOARD',
    sessionCollection: 'sessions',
    assets: {
        lib: {
            css: [
                'public/lib/bootstrap/dist/css/bootstrap.css',
                //'public/lib/bootstrap/dist/css/bootstrap_custom.css',
                'public/lib/bootstrap/dist/css/bootstrap-theme.css',
            ],
            js: [
                'public/lib/angular/angular.js',
                'public/lib/angular-resource/angular-resource.js',
                'public/lib/angular-cookies/angular-cookies.js',
                'public/lib/angular-animate/angular-animate.js',
                'public/lib/angular-touch/angular-touch.js',
                'public/lib/angular-sanitize/angular-sanitize.js',
                'public/lib/angular-ui-router/release/angular-ui-router.js',
                'public/lib/angular-ui-utils/ui-utils.js',
                'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                'public/lib/angular-audio/app/angular.audio.js',
                'public/lib/angular-smart-table/dist/smart-table.js',
                'public/lib/hammerjs/hammer.js',
                'public/lib/angular-gestures/gestures.js',
                'public/lib/angular-easyfb/angular-easyfb.js',
                'public/lib/crypto-js/aes.js',
                'public/lib/angular-socket-io/socket.js',
                'public/lib/socket.io-client/socket.io.js',
                'public/lib/angular-local-storage/dist/angular-local-storage.js'
            ]
        },
        css: [
            'public/modules/**/css/*.css'
        ],
        js: [
            'public/config.js',
            'public/application.js',
            'public/modules/*/*.js',
            'public/modules/*/*[!tests]*/*.js'
        ],
        tests: [
            'public/lib/angular-mocks/angular-mocks.js',
            'public/modules/*/tests/*.js'
        ]
    }
};