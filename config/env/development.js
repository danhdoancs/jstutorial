'use strict';

module.exports = {
	db: 'mongodb://localhost/cloudyboard-dev',
	app: {
		title: 'CloudyBoard - Development Environment'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || '424209677747586',
		clientSecret: process.env.FACEBOOK_SECRET || '9e54add715375ddebf84145c95b1a0be',
		callbackURL: '/auth/facebook/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	}
};
