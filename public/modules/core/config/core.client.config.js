'use strict';
// Setting up route
angular.module('core')
        .constant('CoreConfig', {
            baseUrl: 'http://localhost:3000',
            fbAppId: '424209677747586',
//            baseUrl: 'http://192.168.0.106:3000',
//            fbAppId: '431087833726437',
//        baseUrl: 'http://192.168.0.108:3000',
////                fbAppId: '430678937100660',
//                baseUrl: 'http://192.168.0.100:3000',
//                fbAppId: '436009496567604',
//            baseUrl: 'http://192.168.0.110:3000',
//            fbAppId: '426968287471725',
//            baseUrl: 'http://192.168.0.112:3000',
//            fbAppId: '438458779656009',
//             baseUrl: 'http://192.168.0.103:3000',
//            fbAppId: '429124160589471',
            fbDelay: 1500,
            showLog: true,
            timeRefreshFriendList: 2000, //miliseconds - 30000
            timeUpdateFriendList: 3000, // 20000
            timeReinviteFriend: 10000
        })
        .config(['ezfbProvider', 'CoreConfig',
            function (ezfbProvider, Config) {
                //Init easy-facebook 
                ezfbProvider.setInitParams({
//                    appId: '424209677747586', //IP: .105
//                    appId: '426968287471725', //IP: .100
//                    appId: '428856937282860', //IP: .109
//                    appId: '429124160589471', //IP: .103 
//                    appId: '429403030561584', //IP: 104
//                    appId: '430678937100660', //IP: 108
                    appId: Config.fbAppId,
                    version: 'v2.3'
                });
            }
        ])
        .run(['$rootScope', 'Facebook', 'SocketService',
            function ($rootScope, Facebook, Socket) {
//                Facebook.updateMe();
                Facebook.handleStatusChange();
                //Init playing games flag
                $rootScope.playingGames = [];

                //Init socket
                Socket.setup();
            }
        ]);