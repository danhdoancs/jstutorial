'use strict';

angular.module('core').controller('HeaderController', 
['$rootScope' ,'$scope', 'Menus', 'Window', 'Facebook', 'KeyboardService',
    function ($rootScope ,$scope, Menus, Window, Facebook, ng2048Keyboard) {
        $scope.facebook = Facebook;
        $scope.isCollapsed = false;
        $scope.menu = Menus.getMenu('topbar');

        // Toggle bind for tabs
        $scope.switchBind = function (page) {
            switch (page) {
                case 'home':
                    ng2048Keyboard.unbind();
                    break;
                case '2048':
                    ng2048Keyboard.init();
                    break;
                default:
                    ng2048Keyboard.unbind();
            }
        };

        $scope.toggleCollapsibleMenu = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
        };

        // Collapsing the menu after navigation
        $scope.$on('$stateChangeSuccess', function () {
            $scope.isCollapsed = false;
        });

        // Showup nav
        Window.showup('navbar', {});
    }
]);