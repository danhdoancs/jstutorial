'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Games',
	function($scope, Games) {
            $scope.games = Games;
	}
]);