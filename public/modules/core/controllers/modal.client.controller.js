'use strict';
// Games controller
angular.module('core')
        .controller('InfoModalController', ['$scope', '$modalInstance', 'info',
            function ($scope, $modalInstance, info) {
                $scope.info = info;
                $scope.close = function () {
                    //Close modal
                    $modalInstance.close();
                };
            }]);