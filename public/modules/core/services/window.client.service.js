'use strict';

//Menu service used for managing  menus
angular.module('core').service('Window', ['$window',
    function ($window) {
        this.showup = function (targetClass, options) {
            var target = angular.element(document.getElementsByClassName(targetClass)),
                    upClass = options.upCLass || 'navbar-show',
                    downClass = options.downClass || 'navbar-hide',
                    flagHidden = false,
                    previousOffset = 0,
                    scrollOffset = 20,
                    currentScroll;

            angular.element($window).on('scroll', function () {
                currentScroll = $window.pageYOffset;
                                                       
                if (currentScroll <= scrollOffset) {
                    // Action on scroll up
                    if (flagHidden) {
                        target.removeClass(downClass);
                        flagHidden = false;
                        previousOffset = currentScroll;
                    }
                } else if (currentScroll >= previousOffset) {
                    // Action on scroll down
                    if (!flagHidden) {
                        target.addClass(downClass);
                        flagHidden = true;
                        previousOffset = currentScroll;
                    }
                }
            });

        };
    }
]);