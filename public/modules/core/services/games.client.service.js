'use strict';

//Home service used for managing  games
angular.module('core').factory('Games', [
    function () {
        var games = [
            {
                name: 'Gomoku',
                link: '/#!/gomoku',
                img: 'modules/core/img/games/2048_large.png'
            },
            {
                name: 'Chinese Chess',
                link: '/#!/chinesechess',
                img: 'modules/core/img/games/2048_large.png'
            },
            {
                name: '2048',
                link: '/#!/2048',
                img: 'modules/core/img/games/2048_large.png'
            },
            {
                name: 'Sudoku',
                link: '/#!/sudoku',
                img: 'modules/core/img/games/2048_large.png'
            }
        ];

        return games;
    }
]);