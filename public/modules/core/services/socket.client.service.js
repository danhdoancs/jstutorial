'use strict';

//Games service used to communicate Games REST endpoints
angular.module('core')
        .service('SocketService', ['$http', 'localStorageService',
            'socketFactory', 'GomokuConfig', '$location', 'CoreConfig', 'HelperService', '$modal',
            function ($http, Storage, socketFactory, Config, $location,
                    CoreConfig, Helper, $modal) {
                var self = this;
                var reconnectCount = 0;
                var STORAGE_CONNECTED = 'flagSocketInited';

                this.connectServer = function () {
                    self.socket = socketFactory({
                        prefix: '',
                        ioSocket: io.connect(CoreConfig.baseUrl)
                    });
                    //connect server url
                    $http.get('/socket');
                    //in case user reload the page, reconnect
//                    if (!Storage.get(STORAGE_CONNECTED))
//                    {
//                        //connect server url
//                        $http.get('/socket');
//                        Storage.set(STORAGE_CONNECTED, true);
//                        console.log('Init first connection');
//                    } else {
//                        //send reconnect message
//                        self.socket.emit('reconnect', Storage.get(STORAGE_CONNECTED));
//                    }
                };

//                Facebook.addCallback('login', function () {
//                    //update info
//                    GameManager.players.self.updateInfo({
//                        id: Facebook.me.id,
//                        token: Facebook.authRes.accessToken,
//                        picture: Facebook.me.picture.data.url,
//                        link: Facebook.me.link,
//                        firstName: Facebook.me.first_name,
//                        name: Facebook.me.name
//                    });
//
//                    self.socket.emit('message', 'lgin.' + Facebook.me.id + '#' + Facebook.authRes.accessToken);
//                });

                this.setup = function () {
                    //set up socket
                    this.connectServer();

                    //Connect to the socket.io server!
                    this.onServerMessage();

                    //Disconnect
//                    this.socket.disconnect()
                }; //game_core.constructor

                this.onServerUpdated = function (data) {

                }; //game_core.client_onserverupdate_recieved

                this.onConnected = function (data) {
                    console.log(data);
                    this.socket.id = data;
                }; //client_onconnected

                this.onMessage = function (data) {

                    var commands = data.split('.');
                    var command = commands[0];
                    var commanddata = commands[1] || null;
                    var commanddata2 = commands[2] || null;
                    var commanddata3 = commands[3] || null;

                    switch (command) {
                        case 'inv': //invitation
                            this.onInvited(commanddata, commanddata2);
                            break;
                        case 'repinv': //received response
                            this.onInviteResponse(commanddata2, commanddata3, commanddata);
                            break;
                        case 'cfminv': //confirm invitation
                            this.onConfirmInvitation(commanddata2, commanddata3, commanddata);
                            break;
                    } //subcommand
                }; //client_onnetmessage

                this.onConfirmInvitation = function (game, friendId, confirm) {

//                    console.log('get invite confirm from ' + friendId);
                    if (confirm === '2') { // confirm to play now
                        //game set up                      
                    } else { //confirm to cancle the invitation
                        $modal.open({
                            templateUrl: 'modules/core/views/info.client.modal.html',
                            size: 'md',
                            resolve: {
                                info: function () {
                                    return friendId + " can't play with you right now. Maybe later!"
                                }
                            },
                            controller: 'InfoModalController'
                        });
                    }
                };

                this.onInviteResponse = function (game, friendId, decision) {
                    console.log('get invite response from ' + friendId);

                    if (decision === '1') {

                        $modal.open({
                            templateUrl: 'modules/core/views/invite_accept.client.modal.html',
                            size: 'md',
                            controller: function ($scope, $modalInstance) {
                                $scope.game = game;
                                $scope.friendId = friendId;

                                $scope.play = function () {
                                    //Reply to invitor
                                    self.socket.emit('invite', '2.' + game + '.' + friendId);
                                    //Close modal
                                    $modalInstance.close();
                                };

                                $scope.cancle = function () {
                                    //Reply no to invitor
                                    self.socket.emit('invite', '-2.' + game + '.' + friendId);
                                    //Use the current game
                                    $modalInstance.close();
                                };
                            }
                        });
                    } else {
                        $modal.open({
                            templateUrl: 'modules/core/views/invite_refuse.client.modal.html',
                            size: 'md',
                            controller: function ($scope, $modalInstance) {
                                $scope.game = game;
                                $scope.friendId = friendId;

                                $scope.close = function () {
                                    //Close modal
                                    $modalInstance.close();
                                };
                            }
                        });
                    }

                };

                this.onInvited = function (game, friendId) {
                    $modal.open({
                        templateUrl: 'modules/core/views/invite.client.modal.html',
                        size: 'md',
                        controller: function ($scope, $modalInstance) {
                            $scope.game = game;
                            $scope.friendId = friendId;

                            $scope.yes = function () {
                                //Reply to invitor
                                self.socket.emit('invite', '1.' + game + '.' + friendId);
                                //Close modal
                                $modalInstance.close();
                                console.log('accept invitation from ' + friendId);
                            };

                            $scope.no = function () {
                                //Reply no to invitor
                                self.socket.emit('invite', '-1.' + game + '.' + friendId);
                                //Use the current game
                                $modalInstance.close();
                                console.log('refused invitation from ' + friendId);
                            };
                        }
                    });
                    console.log('receive invitation from ' + friendId);

                };

                this.onDisconnect = function (data) {

                    //this.socket.disconnect();
                    Helper.log('client disconnected');
                    //clear the storage flag
//                    Storage.set(STORAGE_CONNECTED, false);
                }; //client_ondisconnect

                this.onError = function () {
                    //When we disconnect, we don't know if the other player is
                    //connected or not, and since we aren't, everything goes to offline

                    if (Storage.get(STORAGE_CONNECTED) && reconnectCount < 4)
                    {
                        Storage.set(STORAGE_CONNECTED, false);
                        //resetup
//                        this.setup();
                        //reload the page
                        location.reload();
                        //increase count
                        reconnectCount++;

                        Helper.log('got error, client try to reconnect');
                    } else {

                        this.socket.disconnect();
                        Helper.log('got error, client disconnected');
                    }
                };

                this.onServerMessage = function () {

                    //When we connect, we are not 'connected' until we have a server id
                    //and are placed in a game by the server. The server sends us a message for that.
//                    this.socket.once('connect', function () {
//                        GameManager.players.self.state = 'connecting';
//                    }.bind(this));

                    //Sent when we are disconnected (network, server down, etc)
                    this.socket.on('disconnect', this.onDisconnect.bind(this));
                    //Sent each tick of the server simulation. This is our authoritive update
                    this.socket.on('onserverupdate', this.onServerUpdated.bind(this));
                    //Handle when we connect to the server, showing state and storing id's.
                    this.socket.on('onconnected', this.onConnected.bind(this));
                    //On error we just show that we are not connected for now. Can print the data.
                    this.socket.on('error', this.onError.bind(this));
                    //On message from the server, we parse the commands and send it to the handlers
                    this.socket.on('message', this.onMessage.bind(this));
                }; //game_core.client_connect_to_server
            }]);