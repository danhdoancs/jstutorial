'use strict';

//Games service used to communicate Games REST endpoints
angular.module('core')
        .factory('GenerateUniqueId', function () {
            var generateUid = function () {
                var currentDate = new Date().getTime();
                var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    var r = (currentDate + Math.random() * 16) % 16 | 0;
                    currentDate = Math.floor(currentDate / 16);
                    return (c === 'x' ? r : (r & 0x7 | 0x8)).toString(16);
                });

                return uuid;
            };

            return {
                next: function () {
                    return generateUid();
                }
            };
        })
        .service('HelperService', ['CoreConfig', function (Config) {

                //helper to convert x to x,y
                this.positionToCoordinates = function (pos, size) {
                    var x = pos % size;
                    var y = (pos - x) / size;

                    return {x: x, y: y};
                };

                //helper to convert x,y to x
                this.coordinatesToPosition = function (pos, size) {
                    return (pos.y * size) + pos.x;
                };

                this.log = function (msg) {
                    if (Config.showLog)
                        console.log(msg);
                };
            }]);
