'use strict';

//Menu service used for managing  menus
angular.module('core').service('Facebook', ['ezfb', '$q', '$http','HelperService', 'SocketService',
    function (ezfb, $q, $http, Helper, Socket) {
        var self = this,
                callbacks = [],
                defaultImg = 'modules/users/img/facebook-default-avatar.png';
        self.img = defaultImg;
        callbacks['login'] = [];
        callbacks['logout'] = [];
        this.me = {};

        this.addCallback = function (event, callback) {
            callbacks[event].push(callback);
        };
//        /**
//         * Subscribe to 'auth.statusChange' event to response to login/logout
//         */
        this.handleStatusChange = function () {
            ezfb.Event.subscribe('auth.statusChange', function (statusRes) {
                if (statusRes.status === 'connected') {
                    Helper.log(statusRes);
                    self.authRes = statusRes.authResponse;
                    /* 
                     The user is already logged, 
                     is possible retrieve his personal info
                     */
                    // Get my basic info
                    getMe(function () {
                        self.loggedIn = true;
                        //update img
                        self.img = self.me.picture.data.url;
                        //update socket
                        self.me.socketId = Socket.socket.id;
                        /*
                         This is also the point where you should create a 
                         session for the current user.
                         For this purpose you can use the data inside the 
                         res.authResponse object.
                         */
                        serverLogin(self.me, callbacks['login']);
                        //get permission
                        getPermissions(function () {
                            if (self.hasPermission('user_friends')) {
                                getFriends(function () {
                                });
                            }
                        });
                    });
                }
                else {
                    // Delete user data
//                    delete self.me;
                    self.me = {};
                    delete self.friends;
                    delete self.permissions;
                    self.loggedIn = false;
                    self.img = defaultImg;
//                    delete Authentication.user;

                    /*
                     The user is not logged to the app, or into Facebook:
                     destroy the session on the server.
                     */
                    serverLogout(callbacks['logout']);
                }
            });
        };

        this.onSendBrag2048 = function (score, tile) {
            sendBrag({
                caption: 'Game 2048 on CloudyBoard',
                name: 'CloudyBoard is getting fun! ',
                picture: 'http://s4.postimg.org/bwkwmpj0t/2048_large.png',
                link: 'http://192.168.0.105:3000/#!/',
                description: 'I just got to tile ' + tile + ' with ' + score + ' points in game 2048 on CloudyBoard, think you can beat me?'
            }, null);
        };

        function sendBrag(options, callback) {
            ezfb.ui({
                method: 'feed',
                caption: options.caption,
                name: options.name,
                picture: options.picture,
                link: options.link,
                description: options.description
            }, callback);
        }

        this.share = function (options, callback) {
            ezfb.ui({
                method: 'share',
                caption: options.caption,
                name: options.name,
                picture: options.picture,
                link: options.link,
                description: options.description
            }, callback);
        };

        this.onChallenge = function () {
            sendChallenge(null, 'Hey, game 2048 on CloudyBoard is getting really fun. Come check it out!',
                    function (response) {
                        Helper.log('sendChallenge', response);
                    });
        };

        function sendChallenge(to, message, callback) {
            var options = {
                method: 'apprequests'
            };
            if (to)
                options.to = to;
            if (message)
                options.message = message;
            ezfb.ui(options).then(function (response) {
                if (callback)
                    return callback(response);
            });
        }

        //Re request
        this.reRequestFriends = function (callback) {
            reRequest('user_friends', function () {
                //get permission
                getPermissions(function () {
                    if (self.hasPermission('user_friends')) {
                        getFriends(function () {
                            callback();
                        });
                    }
                });
            });
        };

        function reRequest(scope, callback) {
            ezfb.login(callback, {scope: scope, auth_type: 'rerequest'});
        }

        //Get permissions
        function getPermissions(callback) {
            if (self.loggedIn) {
                ezfb.api('/me/permissions').then(function (response) {
                    if (!response.error) {
                        self.permissions = response.data;
                        callback();
                    } else {
                        console.error('/me/permissions', response);
                    }
                });
            }
        }

        //Has permissions check
        this.hasPermission = function (permission) {
            for (var i in self.permissions) {
                if (self.permissions[i].permission === permission &&
                        self.permissions[i].status === 'granted') {
                    return true;
                }
            }
            return false;
        };

        //Get friends
        function getFriends(callback) {
            ezfb.api('/me/friends', {fields: 'id,name,picture.width(120).heigh(120)'})
                    .then(function (friends) {
                        if (friends && !friends.error) {
                            // Get game info of friends
                            self.friends = friends.data;
                            callback();
                        }
                    });
        }

        //Get me
        function getMe(callback) {
            ezfb.api('/me', {fields: 'id,email,name,first_name,last_name,picture.width(120).heigh(120),updated_time.date_format(Ymd),gender,link'})
                    .then(function (me) {
                        // Save user into brower window data
                        self.me = me;
//                        Helper.log(me);
                        callback();
                    });
        }

        this.getUser = function (id, token, callback) {
//            console.log(self.authRes.accessToken);
            ezfb.api(
                    "/" + id,
                    {fields: 'name,first_name,picture.width(120).heigh(120),link',
                        access_token: token},
            function (response) {
                /* handle the result */
                if (callback)
                    callback(response);
            });
        };

        this.login = function (callback) {
            ezfb.login(function (response) {
                if (response.authResponse) {
                    //set id first
                    self.me.id = response.authResponse.userID;
                    //run run time callback
//                    console.log(response.authResponse.userId);
                    if (callback)
                        callback();
                }
            }, {scope: 'email,user_friends'});
        };

        this.logout = function () {
            ezfb.logout();
        };

        function serverLogin(user, callbacks) {
            $http.post('/auth/facebook/login', {user: user})
                    .success(function () {
                        for (var i = 0; i < callbacks.length; i++)
                            callbacks[i]();
                    });
        }

        function serverLogout(callbacks) {
            $http.get('/auth/facebook/logout').success(function () {
                for (var i = 0; i < callbacks.length; i++)
                    callbacks[i]();
            });
        }
    }
]);