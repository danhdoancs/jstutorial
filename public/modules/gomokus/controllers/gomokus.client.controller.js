'use strict';

// Gomokus controller
angular.module('gomokus').controller('GomokuController',
        ['$rootScope', '$scope', '$modal', '$stateParams', '$location', 'GomokuGameManager',
            'Facebook', 'GomokuModel', 'GomokuCoordinator', 'HelperService',
            'GomokuStorage',
            function ($rootScope, $scope, $modal, $stateParams, $location, Game, Facebook,
                    Gomoku, Coordinator, Helper, Storage) {
                //if user directly access this page, redirect to the menu page
                if (!Game.flagPlayable && !Storage.storage.get('flagPlayable')) {
                    return $location.path('gomoku');
                }

                $scope.game = Game;
                $scope.facebook = Facebook;
//                $scope.players = Game.players;

                function newGame() {
                    if (Facebook.loggedIn) {
                        Game.userGameSetup();
                    } else {
                        Game.setup();
                    }

                    Game.flagFindingOpponent = false;
                    Game.flagToggleFriendList = false;
                    Game.flagRequestingFriend = false;
                    Game.flagAskingAnotherGame = false;
                }

                $scope.done = function () {
                    Game.isDone = true;
                    Game.players.self.isWinner = false;
                    //send done request to server
                    Coordinator.onFinishGame();
                };

                $scope.quit = function () {
                    return $location.path('gomoku');
                };

                $scope.continue = function () {
//                    Game.continueGame();
                    //ask other player to play another round
                    if (!Game.flagAskingAnotherGame) {
                        Coordinator.askAnotherGame();
                        //ask one time
                        Game.flagAskingAnotherGame = true;
                    }
                };

                $scope.move = function (cellPos) {
                    var player = Game.players.self;
                    //check if can move
                    if (!player.myTurn || Game.gameOver)
                        return false;

                    var cellCoor = Helper.positionToCoordinates(cellPos, Game.gameSize);

                    Game.playerMove(player, cellPos, cellCoor);
                    Coordinator.onMyMove(cellPos);

                    // Check game done
                    var result = Game.checkGameDone(cellCoor, player);
                    console.log('result: ' + result);
                    //send result to server
                    if (result > -1) {
                        Coordinator.sendGameResult(result);
                        console.log("game over, send done request to server");
                        //clear storage
                        Storage.clearGameState();
                    } else {
                        //save move to storage
                        Storage.setGameState(Game.serialize());
                    }
                };

                $scope.share = function () {
                    Facebook.share({
                        caption: 'Game Gomoku on CloudyBoard',
                        name: 'CloudyBoard is getting fun! ',
                        picture: 'http://s4.postimg.org/bwkwmpj0t/2048_large.png',
                        link: 'http://192.168.0.103:3000/#!/',
                        description: 'I just beat a guy in game Gomoku on CloudyBoard, who\' next?'
                    }, null);
                };

                function setupUserGame() {
                    if (Facebook.loggedIn) {
                        //Setup game for new user
                        Game.userGameSetup();
//                      //modal to ask user to load old game
                        //only when there is a local old game
                        if (Game.userOldGame)
                            //Reload user old game without asking 
                            Game.reloadUserOldGame();

                    }
                }

                //load new game
                if (!$rootScope.playingGames.gomoku) {
                    newGame();
                    $rootScope.playingGames.gomoku = true;
                }

                //set up callback for facebook status changes
                Facebook.addCallback('login', setupUserGame);
                Facebook.addCallback('logout', function () {
                    return $location.path('/');
                });
            }
        ]);