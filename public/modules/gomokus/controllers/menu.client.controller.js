'use strict';

// Gomokus controller
angular.module('gomokus').controller('GomokuMenuController',
        ['$scope', '$modal', '$stateParams', '$location', 'GomokuGameManager', 'localStorageService',
            'Facebook', 'GomokuModel', 'GomokuCoordinator', 'Leaders', 'Friends', 'CoreConfig', '$timeout', '$rootScope',
            function ($scope, $modal, $stateParams, $location, Game, Storage, Facebook, Gomoku,
                    Coordinator, Leaders, Friends, Config, $timeout) {
                $scope.game = Game;
                $scope.Facebook = Facebook;
//                Game.flagChallenge = false;
                Game.flagFindingOpponent = false;
                Game.flagToggleFriendList = false;
                Game.flagRequestingFriends = {};
                Game.flagRequestingFriend = false;
                $scope.countRequestChallenge = 0;

                var timeLoadFriends = 0;
                var MAX_REQUEST_ALLOWED = 10;

                //set flag Playable = false
                Game.flagPlayable = false;
                Storage.set('flagPlayable', false);

                $scope.inviteFriend = function (friendSocketId) {
                    if (!Game.flagToggleFriendList || Game.flagFindingOpponent)
                        return false;

                    //alow invite one friend at a time
                    if (Game.flagRequestingFriend) {
                        $modal.open({
                            templateUrl: 'modules/core/views/info.client.modal.html',
                            size: 'md',
                            controller: 'InfoModalController',
                            resolve: {
                                info: function () {
                                    return "You only can invite a friend at a time.<br/>Please wait until your friend response first.";
                                }
                            }
                        });
                        return false;
                    }
                    ;

                    Coordinator.inviteFriend(friendSocketId);

                    Game.flagRequestingFriends[friendSocketId] = true;
                    Game.flagRequestingFriend = true;

                    //open the lock for a period of inviting time
                    $timeout(function () {
                        Game.flagRequestingFriends[friendSocketId] = false;
                        Game.flagRequestingFriend = false;
                    }, Config.timeReinviteFriend);
                };

                $scope.playWithFriend = function () {
                    //in case close the friend tab, do nothing
                    if (Game.flagToggleFriendList) {
                        Game.flagToggleFriendList = false;
                        return;
                    }

                    //turn off finding opponent if are
                    if (Game.flagFindingOpponent) {
                        $scope.challengeTheWorld();
                    }
//                    Game.clearHistory();
                    //only if user logged in
                    if (!Facebook.loggedIn) {
                        Facebook.login(function () {
                            setTimeout(function () {
                                //update self player
                                Game.players.self.id = Facebook.me.id;
                                //send id to sever first
                                Coordinator.socket.emit('message', 'friendlist.' + Facebook.me.id);
                                //set flag
                                Game.flagToggleFriendList = !Game.flagToggleFriendList;

                                getFriendList();
                            }, 1500);
                        });
                    } else {
                        //set flag
                        Game.flagToggleFriendList = !Game.flagToggleFriendList;

                        getFriendList();
                    }
                    
                    updateFriendList();
                };

                $scope.challengeTheWorld = function () {
                    Game.clearHistory();
                    //only if user logged in
                    if (!Facebook.loggedIn) {
                        Facebook.login(function () {
                            setTimeout(function () {
                                //update self player
                                Game.players.self.id = Facebook.me.id;
                                //send id to sever first
                                Coordinator.socket.emit('message', 'lgin.' + Facebook.me.id);
                                //send challenge request
                                if (Game.flagFindingOpponent) {
                                    Coordinator.socket.emit('message', 'ch.0');
                                } else if ($scope.countRequestChallenge <= MAX_REQUEST_ALLOWED) {
                                    //Send challenge request to server
                                    Coordinator.socket.emit('message', 'ch.1');
                                }
                                //set flag
                                Game.flagFindingOpponent = !Game.flagFindingOpponent;
                                //increase count
                                $scope.countRequestChallenge++;
                            }, 1500);
                        });
                    } else {
                        if (Game.flagFindingOpponent) {
                            Coordinator.socket.emit('message', 'ch.0');
                        } else if ($scope.countRequestChallenge <= MAX_REQUEST_ALLOWED) {
                            //Send challenge request to server
                            Coordinator.socket.emit('message', 'ch.1');
                        }
                        //set flag
                        Game.flagFindingOpponent = !Game.flagFindingOpponent;
                        //increase count
                        $scope.countRequestChallenge++;
                    }
                };

                // Reask friend list permission, then get top friends records
                $scope.reRequestFriends = function () {
                    Facebook.reRequestFriends(function () {
                        timeLoadFriends = new Date().getTime();
                        getFriendList();
                    });
                };

                function updateFriendList() {
                    //stop when friend list panel 
                    if (!Game.flagToggleFriendList)
                        return;
                    //recursive
                    $timeout(function () {
                        //update
                        getFriendList();
                        //repeat
                        updateFriendList();
                    }, Config.timeUpdateFriendList);
                }

                function getFriendList() {
//                    timeLoadFriends = ;
                    //limit time between requests is 2 secs
                    var now = new Date().getTime();
                    if (now - timeLoadFriends > Config.timeRefreshFriendList) {
                        if (Facebook.friends) {
//                            var friendIds = [];
//                            for (var i = 0; i < Facebook.friends.length; i++) {
//                                friendIds[i] = Facebook.friends[i].id;
//                            }
                            var friendIds = Facebook.friends.map(function (friend) {
                                return friend.id;
                            });

                            $scope.friends = Friends.get({ids: friendIds});

                        }
                        timeLoadFriends = now;
                        console.log('update friends');
                    }
                }

                $scope.backToMenu = function () {
                    //delete game if finding opponent
                    if (Game.flagFindingOpponent) {
                        Coordinator.socket.emit('message', 'ch.0');
                        Game.flagFindingOpponent = false;
                    }

                    $location.path('/#!/');
                };
            }
        ]);