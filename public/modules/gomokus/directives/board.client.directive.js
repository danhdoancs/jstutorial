'use strict';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('gomokus').directive('gomokuboard', function(){
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            ngModel: '='
        },
        controller: 'GomokuController',
        templateUrl: 'modules/gomokus/views/board.client.template.html'
    };
});


