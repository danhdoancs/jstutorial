'use strict';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('gomokus').directive('gomokutile', function(){
    
    return {
        restrict: 'E',
        scope: {
            ngModel: '='
        },
        templateUrl: 'modules/gomokus/views/tile.client.template.html'
    };
});


