'use strict';

//Gomokus service used to communicate Gomokus REST endpoints
angular.module('gomokus')
        .factory('GomokuModel', ['$resource',
            function ($resource) {
                return $resource('gomoku/:gomokuId', {gomokuId: '@_id'
                }, {
                    update: {
                        method: 'PUT'
                    }
                });
            }
        ])
        .factory('Leaders', ['$resource',
            function ($resource) {
                return $resource('2048/leaderboard/:category', null, {
                    toppers: {
                        method: 'GET',
                        params: {category: 'toppers'},
                        isArray: true,
                        // Change cache: true when the database is big
                        cache: false
                    },
                    friends: {
                        method: 'POST',
                        params: {category: 'friends'},
                        isArray: true,
                        cache: true
                    }
                });
            }]);