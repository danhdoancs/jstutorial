'use strict';
//Games service used to communicate Games REST endpoints
angular.module('gomokus')
        .provider('GomokuBoardProvider', ['GomokuConfig', function (Config) {
                var thisService = this;

                //size of the board
                this.size = Config.BOARD_SIZE;
                this.totalSize = this.size * this.size;
                this.availableCells = this.totalSize;

                this.setSize = function (newSize) {
                    this.size = newSize ? newSize : Config.BOARD_SIZE;
                    this.totalSize = this.size * this.size;
                };

                this.$get = function (GomokuTileModel, HelperService) {
                    this.tiles = [];
                    this.grid = [];

                    this.getSize = function () {
                        return this.size;
                    };

                    //load saved game
                    this.initFromState = function (state) {
                        //restore save tiles
                        for (var x = 0; x < this.size; x++) {
                            for (var y = 0; y < this.size; y++) {
                                var pos = y * this.size + x;
                                var tile = state[x][y];
                                //init grid
                                thisService.grid[pos] = null;
                                //restore tile
                                this.tiles[pos] = tile ? this.newTile(tile.position, tile.value) : null;
                            }
                        }
                    };

                    //build game board
                    this.buildEmptyGameBoard = function () {
                        //initialize our grid
                        for (var x = 0; x < thisService.totalSize; x++) {
                            thisService.grid[x] = null;
                        }

                        this.forEach(function (x, y) {
                            thisService.setCellAt({x: x, y: y}, null);
                        });
                    };

                    //is the position within the grid
                    this.withinGrid = function (cell) {
                        return cell.x >= 0 && cell.x < this.size &&
                                cell.y >= 0 && cell.y < this.size;
                    };
                    //is a cell available at a position
                    this.cellAvailable = function (cell) {
                        if (this.withinGrid(cell)) {
                            return !this.getCellAt(cell);
                        } else {
                            return false;
                        }
                    };

                    //check to see there are still available cells
                    this.anyCellsAvailable = function () {
                        return this.availableCells > 0;
                    };

                    //get a cell at a position
                    this.getCellAt = function (pos) {
                        if (this.withinGrid(pos)) {
                            var x = HelperService.coordinatesToPosition(pos, this.size);
                            return this.tiles[x];
                        } else {
                            return null;
                        }
                    };

                    this.setCellAt = function (pos, tile) {
//                        if (this.withinGrid(pos)) {
                        var xPos = HelperService.coordinatesToPosition(pos, this.size);
                        this.tiles[xPos] = tile;
//                        }
                    };

                    //run a callback on every cell either on a grid or tile
                    this.forEach = function (cb) {
                        for (var i = 0; i < this.totalSize; i++) {
                            var coordinate = HelperService.positionToCoordinates(i, thisService.size);
                            cb(coordinate.x, coordinate.y, this.tiles[i]);
                        }
                    };


                    //insert a new tile
                    this.insertNewTile = function (cellPos, cellCoor, value) {
//                        var pos = HelperService.coordinatesToPosition(tile, this.size);
                        this.tiles[cellPos] = this.newTile(cellCoor, value);
                        //count available cells
                        this.availableCells--;
                    };

                    this.newTile = function (pos, value) {
                        return new GomokuTileModel(pos, value);
                    };

                    this.serialize = function () {
                        var cellState = [];

                        for (var x = 0; x < this.size; x++) {
                            var row = cellState[x] = [];

                            for (var y = 0; y < this.size; y++) {
                                var tile = this.tiles[y * this.size + x];
                                row.push(tile ? tile.serialize() : null);
                            }
                        }

                        return {
                            size: this.size,
                            cells: cellState
                        };
                    };

                    //set new to false
                    this.unflagNewTile = function (cellPos) {
                        if (cellPos && this.tiles[cellPos])
                            this.tiles[cellPos].new = false;
                    };

                    //set tile flag winning to true
                    this.flagWinningTiles = function (five) {
                        for (var i = 0; i < five.length; i++)
                            this.tiles[five[i]].isWinningTile = true;
                    };

                    //check win
                    //return: array of 5 winning tile indeces or false
                    this.checkWin = function (cellCoor, value) {
                        var five = [],
                                size = this.size,
                                tiles = this.tiles;
                        //check horizontal line
                        var startTile = cellCoor.y * size;
                        var endTile = startTile + size;
                        for (var i = startTile; i < endTile; i++) {
                            //check if next tile is on
                            if (tiles[i] && tiles[i].value === value) {
                                // got more than 5 in a row, not win
                                if (five.length > 4)
                                    return false;
                                //increase
                                five.push(i);
                            } else {
                                //got 5 in a row and the next tile is opponent's or null
                                if (five.length === 5)
                                    return five;
                                //reset
                                five = [];
                            }
                        }
                        //check case 5 tiles in next to wall
                        if (five.length === 5)
                            return five;
                        //reset
                        five = [];

                        //check vertical line
                        startTile = cellCoor.x;
                        for (i = startTile; i < this.totalSize; i += size) {
                            //check if next tile is on
                            if (tiles[i] && tiles[i].value === value) {
                                // got more than 5 in a row, not win
                                if (five.length > 4)
                                    return false;
                                //increase
                                five.push(i);
                            } else {
                                //got 5 in a row and the next tile is opponent's or null
                                if (five.length === 5)
                                    return five;
                                //reset
                                five = [];
                            }
                        }
                        //check case 5 tiles in next to wall
                        if (five.length === 5)
                            return five;
                        //reset
                        five = [];

                        //check positive diagonal line
                        //line in IV part
                        var offset = size - 1;
                        if (cellCoor.x + cellCoor.y >= size - 1) {
                            startTile = size * (cellCoor.y - (size - cellCoor.x - 1)) + offset;
                            for (i = startTile; i < this.totalSize; i += offset) {
                                //check if next tile is on
                                if (tiles[i] && tiles[i].value === value) {
                                    // got more than 5 in a row, not win
                                    if (five.length > 4)
                                        return false;
                                    //increase
                                    five.push(i);
                                } else {
                                    //got 5 in a row and the next tile is opponent's or null
                                    if (five.length === 5)
                                        return five;
                                    //reset
                                    five = [];
                                }
                            }
                            //check case 5 tiles in next to wall
                            if (five.length === 5)
                                return five;
                            //reset
                            five = [];
                            //check negative diagonal line
                        } else { // line in  II part
                            startTile = (cellCoor.x + cellCoor.y) * size;

                            for (i = startTile; i > -1; i -= offset) {
                                //check if next tile is on
                                if (tiles[i] && tiles[i].value === value) {
                                    // got more than 5 in a row, not win
                                    if (five.length > 4)
                                        return false;
                                    //increase
                                    five.push(i);
                                } else {
                                    //got 5 in a row and the next tile is opponent's or null
                                    if (five.length === 5)
                                        return five;
                                    //reset
                                    five = [];
                                }
                            }
                            //check case 5 tiles in next to wall
                            if (five.length === 5)
                                return five;
                            //reset
                            five = [];
                        }

                        //check negative diagonal line
                        //line in I part
                        offset = size + 1;
                        if (cellCoor.y - cellCoor.x < 0) {
                            startTile = size * (cellCoor.y + size - cellCoor.x - 1) + size - 1;
                            for (i = startTile; i > -1; i -= offset) {
                                //check if next tile is on
                                if (tiles[i] && tiles[i].value === value) {
                                    // got more than 5 in a row, not win
                                    if (five.length > 4)
                                        return false;
                                    //increase
                                    five.push(i);
                                } else {
                                    //got 5 in a row and the next tile is opponent's or null
                                    if (five.length === 5)
                                        return five;
                                    //reset
                                    five = [];
                                }
                            }
                            //check case 5 tiles in next to wall
                            if (five.length === 5)
                                return five;
                            //reset
                            five = [];
                            //check negative diagonal line
                        } else { // line in  III part
                            startTile = (cellCoor.y - cellCoor.x) * size;
                            for (i = startTile; i < this.totalSize; i += offset) {
                                //check if next tile is on
                                if (tiles[i] && tiles[i].value === value) {
                                    // got more than 5 in a row, not win
                                    if (five.length > 4)
                                        return false;
                                    //increase
                                    five.push(i);
                                } else {
                                    //got 5 in a row and the next tile is opponent's or null
                                    if (five.length === 5)
                                        return five;
                                    //reset
                                    five = [];
                                }
                            }
                            //check case 5 tiles in next to wall
                            if (five.length === 5)
                                return five;
                            //reset
                            five = [];
                        }

                        return false;
                    };

                    return this;
                };
            }]);