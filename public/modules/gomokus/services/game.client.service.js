'use strict';

//Games service used to communicate Games REST endpoints
angular.module('gomokus').service('GomokuGameManager',
        ['GomokuStorage', 'GomokuBoardProvider', 'Sounds', 'GomokuModel', 'Facebook', 'GomokuConfig',
            'HelperService', 'GomokuPlayer', 'localStorageService',
            function (Storage, Board, Sounds, Gomoku, Facebook, Config, Helper, Player, LocalStorage) {

                var self = this;
                this.grid = Board.grid;
                this.tiles = Board.tiles;
                this.gameSize = Board.getSize();
                this.winningValue = 32;
                this.setNewRecord = false;
                this.highScore = 0;
                this.highTile = 2;
                this.currentScore = 0;
                this.gameOver = false;
                this.gameCount = 1;
                this.players = {};

                this.flagFindingOpponent = false;

                //create new players
                initPlayers();

                // Load button sound first
//                Sounds.load('button', 'modules/ng2048s/sounds/button.mp3');

                //innit self player after logged in
                function initPlayers() {
                    //We create a player set, passing them
                    //the game that is running them, as well
                    //check local storage first
                    self.players = {
                        self: new Player(),
                        other: new Player()
                    };
                }

                this.initSounds = function () {
                    //load basic sound effects
                    Sounds.load('moving', 'modules/ng2048s/sounds/moving.mp3');
                    Sounds.load('merging', 'modules/ng2048s/sounds/merging.mp3');
                    Sounds.load('combo', 'modules/ng2048s/sounds/combo.mp3');
                    Sounds.load('gameover', 'modules/ng2048s/sounds/gameover.mp3');
                    Sounds.load('gamewin', 'modules/ng2048s/sounds/gamewin.mp3');
                };

                this.restartGame = function () {
                    //clear storage
                    Storage.clearGameState();
                    this.refresh();
                    //Play sound
//                    Sounds.play('button');
                    Helper.log('restarted new game');
                };

                this.refresh = function () {
                    this.gameOver = false;
                    this.isDone = false;
                    this.forceQuit = false;
                    this.win = false;
                    this.gameCount++;
                    this.players.self.isWinner = false;
                    this.players.other.isWinner = false;

                    //switch first turn for another 
                    if ((this.gameCount % 2) === 0)
                    {
                        this.players.host.myTurn = false;
                        this.players.guest.myTurn = true;
                    } else {
                        this.players.host.myTurn = true;
                        this.players.guest.myTurn = false;
                    }

                    //reset up the board
                    Board.buildEmptyGameBoard();
                };

                this.clearHistory = function () {
                    //clear the local storage other player infor
                    LocalStorage.remove('player_other');
                    LocalStorage.remove('player_self');

                    this.gameOver = false;
                    this.isDone = false;
                    this.forceQuit = false;
                    this.win = false;
                    this.gameCount = 1;
                    
                    //reset players
                    this.players.self.resetStatus();
                    this.players.other.resetStatus();

                    //reset up the board
                    Board.buildEmptyGameBoard();
                };

                this.reloadUserOldGame = function () {
                    if (this.userOldGame) {
                        Board.initFromState(this.userOldGame.grid.cells); // Reload grid
                        this.currentScore = this.userOldGame.score;
                        this.gameOver = this.userOldGame.over;
                        this.win = this.userOldGame.won;
                        this.gameCount = this.userOldGame.gameCount;
                        this.players = this.userOldGame.players;
//                        this.players.other.myTurn = this.userOldGame.players.other.myTurn;
                    }
                };

                this.clearUserOldGame = function () {
//                    StorageManager.clearGameState();
                };

                //set up when have user
                this.userGameSetup = function () {
                    if (Facebook.loggedIn) {
                        //Update storage user
                        Storage.setUser(Facebook.me.id);
                        this.userOldGame = Storage.getGameState();
                    }
                };

                //load old game or build new game
                this.setup = function () {
                    //reset up the board
                    Board.buildEmptyGameBoard();
                    this.gameOver = false;
                    // Clear the userOldgame
//                    this.userOldGame = null;
                };

                //Handle the move action
                this.playerMove = function (player, cellPos, cellCoor) {

                    //Player move
                    player.onMove(cellPos);

                    //Insert tile
                    Board.insertNewTile(cellPos, cellCoor, player.color);

                    //switch turn
                    if (player === this.players.self)
                        this.players.other.myTurn = true;
                    else
                        this.players.self.myTurn = true;
                    player.myTurn = false;

                    //unflag self new tile
                    Board.unflagNewTile(player === this.players.self ?
                            this.players.other.lastTilePos :
                            this.players.self.lastTilePos);

                    //save move to storage

                };
                
                //Are there moves left?
                this.movesAvailable = function () {
                    return Board.anyCellsAvailable();
                };

                //Check game done
                this.checkGameDone = function (movingCoor, player) {
                    var five = Board.checkWin(movingCoor, player.color);
                    var other = self.players.self === player ? self.players.other : self.players.self;
                    if (five) {
                        Board.flagWinningTiles(five);
                        player.isWinner = 1;
                        player.totalWons++;
                        other.isWinner = -1;
                        self.gameOver = true;
                        return 1;
                    }

                    //Check game over
                    if (!self.movesAvailable()) {
                        self.gameOver = true;
                        player.isWinner = 0;
                        other.isWinner = 0;
                        self.gameOver = true;
                        return 0;
                        // Clear the state when the game is over (game over only, not win)
//                        Storage.clearGameState();
                        //Play sound
//                            Sounds.play('gameover');
                    } else {
//                        Storage.setGameState(self.serialize());
                    }
                    return -1;
                };

                // Represent the current game as an object
                this.serialize = function () {
                    return {
                        grid: Board.serialize(),
                        over: this.gameOver,
                        won: this.isWinner,
                        gameCount: this.gameCount,
                        players: this.players,
//                            player_self: this.players.self.serialize(),
//                        player_other: this.players.other.serialize(),
                    };
                };

            }]);