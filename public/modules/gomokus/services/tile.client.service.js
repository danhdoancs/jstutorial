'use strict';

//Games service used to communicate Games REST endpoints
angular.module('gomokus')
        .factory('GomokuTileModel', function () {
            var Tile = function (pos, value) {
                this.x = pos.x;
                this.y = pos.y;
                this.value = value || 0;

//                this.id = 1;
                this.new = true;
            };

            Tile.prototype.reset = function () {
                this.new = null;
            };

            Tile.prototype.getPosition = function () {
                return {
                    x: this.x,
                    y: this.y
                };
            };

            Tile.prototype.serialize = function () {
                return {
                    position: {
                        x: this.x,
                        y: this.y
                    },
                    value: this.value
                };
            };

            return Tile;
        });