'use strict';

//Games service used to communicate Games REST endpoints
angular.module('gomokus')
        .service('GomokuCoordinator', ['$http', 'Facebook', 'localStorageService', 'GomokuGameManager',
            'SocketService', 'GomokuConfig', '$location', 'CoreConfig', 'HelperService',
            function ($http, Facebook, Storage, GameManager, Socket, Config, $location,
                    CoreConfig, Helper) {
                var self = this;
                this.socket = Socket.socket;
                var reconnectCount = 0;
                var STORAGE_CONNECTED = 'flagSocketInited';

                Facebook.addCallback('login', function () {
                    //update info
                    GameManager.players.self.updateInfo({
                        id: Facebook.me.id,
                        token: Facebook.authRes.accessToken,
                        picture: Facebook.me.picture.data.url,
                        link: Facebook.me.link,
                        firstName: Facebook.me.first_name,
                        name: Facebook.me.name
                    });

                    self.socket.emit('message', 'lgin.' + Facebook.me.id + '#' + Facebook.authRes.accessToken);
                });

                this.setup = function () {
                    //Connect to the socket.io server!
                    this.onServerMessage();

                    //Disconnect
//                    this.socket.disconnect()
                }; //game_core.constructor

                this.onMyMove = function (cellPos) {
                    //Send to server
                    this.socket.emit('move', {
                        from: GameManager.players.self.isHost ? 'host' : 'guest',
                        pos: cellPos
                    });
                };

                this.onOtherPlayerMove = function (cellPos) {
                    var cellCoor = Helper.positionToCoordinates(cellPos, GameManager.gameSize);
                    var player = GameManager.players.other;
                    GameManager.playerMove(player, cellPos, cellCoor);

                    // Check game done
                    var result = GameManager.checkGameDone(cellCoor, player);
                    //send result to server
                    if (result === 1)
                        self.sendGameResult(-1);
                    else if (result === 0)
                        self.sendGameResult(0);
                };

                this.inviteFriend = function (socketId) {
                    this.socket.emit('invite', '0.gomoku.' + socketId);
                };

                this.sendGameResult = function (flagWinner) {
                    this.socket.emit('end', flagWinner);
                };

                this.askAnotherGame = function () {
                    this.socket.emit('message', 'more');
                };

                this.onServerUpdated = function (data) {

                }; //game_core.client_onserverupdate_recieved

                this.onReadyGame = function (data) {

                    //set facebook id for opponent
                    var datas = data.split('#');

                    //get opponent info
                    Facebook.getUser(datas[0], datas[1], function (player) {
                        //error trying to get player info
                        if (!player || player.error) {
                            //cancle the game, call server to match another player
                            self.socket.emit('message', 'ch.1');
                            return;
                        }

                        GameManager.players.other.updateInfo({
                            id: datas[0],
                            token: datas[1],
                            picture: player.picture.data.url,
                            link: player.link,
                            firstName: player.first_name,
                            name: player.name
                        });

                        //save player to local storage
                        Storage.set('player_self', GameManager.players.self);
                        Storage.set('player_other', GameManager.players.other);
                    });

                    Helper.log('game ready: ' + data);

                    //redirect player to playing page
                    $location.path('gomoku/play');
                    GameManager.flagPlayable = true;
                    Storage.set('flagPlayable', true);
                }; //client_onreadygame

                this.onJoinGame = function (data) {
                    GameManager.players.self.onJoinGame();
                    GameManager.players.other.onHostGame();
                    //set host and guest
                    GameManager.players.host = GameManager.players.other;
                    GameManager.players.guest = GameManager.players.self;
                    //Log
                    Helper.log('connected.joined.wating');
                }; //client_onjoingame

                this.onHostGame = function (data) {
                    GameManager.players.self.onHostGame();
                    GameManager.players.other.onJoinGame();
                    GameManager.players.host = GameManager.players.self;
                    GameManager.players.guest = GameManager.players.other;
                    //log
                    Helper.log('hosting.wating for a player');
                }; //client_onhostgame

                this.onConnected = function (data) {
                    GameManager.players.self.onConnected(data);
                }; //client_onconnected

                this.onMessage = function (data) {
                    console.log('receive message ' + data);
                    var commands = data.split('.');
                    var command = commands[0];
                    var commanddata = commands[1] || null;

                    switch (command) {

                        case 'h' : //host a game requested
                            this.onHostGame(commanddata);
                            break;
                        case 'j' : //join a game requested
                            this.onJoinGame(commanddata);
                            break;
                        case 'r' : //ready a game requested
                            this.onReadyGame(commanddata);
                            break;
                        case 'm': //play another game
                            this.onAnotherGame();
                            break;
                        case 'e' : //end game requested
                            this.onEndGame(commanddata);
                            break;
                        case 'd' : //other player disconnected
                            this.onDisconnect(commanddata);
                            break;
                        case 'rs': //restart new game
                            this.onRestartGame();
                            break;
                        case 'repinv': //received response
                            this.onInviteResponse();
                            break;

                    } //subcommand
                }; //client_onnetmessage
                
                this.onInviteResponse = function() {
//                    GameManager.flagRequestingFriend = false;
                };

                this.onRestartGame = function () {
                    GameManager.restartGame();
                };

                this.onFinishGame = function () {
                    this.socket.emit('message', 'finish.');
                };

                this.onDisconnect = function (data) {

                    //When we disconnect, we don't know if the other player is
                    //connected or not, and since we aren't, everything goes to offline
                    GameManager.players.self.state = 'not-connected';
                    GameManager.players.self.online = false;

                    GameManager.players.other.state = 'not-connected';

                    //this.socket.disconnect();
                    Helper.log('client disconnected');
                }; //client_ondisconnect

                this.onEndGame = function (data) {
                    //other player quit, we quit too
                    GameManager.isDone = true;
                    GameManager.forceEnd = true;
                    GameManager.gameOver = true;
                    GameManager.players.self.isWinner = false;
                    Helper.log('game done, totalWons: ' + GameManager.players.self.totalWons);
                };

                this.onError = function () {
                    //When we disconnect, we don't know if the other player is
                    //connected or not, and since we aren't, everything goes to offline

                    if (Storage.get(STORAGE_CONNECTED) && reconnectCount < 4)
                    {
                        Storage.set(STORAGE_CONNECTED, false);
                        //resetup
//                        this.setup();
                        //reload the page
                        location.reload();
                        //increase count
                        reconnectCount++;

                        Helper.log('got error, client try to reconnect');
                    } else {

                        GameManager.players.self.state = 'not-connected';
                        GameManager.players.self.online = false;

                        GameManager.players.other.state = 'not-connected';

                        this.socket.disconnect();
                        Helper.log('got error, client disconnected');
                    }
                };

                this.onServerMessage = function () {

                    //When we connect, we are not 'connected' until we have a server id
                    //and are placed in a game by the server. The server sends us a message for that.
//                    this.socket.once('connect', function () {
//                        GameManager.players.self.state = 'connecting';
//                    }.bind(this));

                    //Sent when we are disconnected (network, server down, etc)
                    this.socket.on('disconnect', this.onDisconnect.bind(this));
                    //Sent each tick of the server simulation. This is our authoritive update
                    this.socket.on('onserverupdate', this.onServerUpdated.bind(this));
                    //Handle when we connect to the server, showing state and storing id's.
                    this.socket.on('onconnected', this.onConnected.bind(this));
                    //On error we just show that we are not connected for now. Can print the data.
                    this.socket.on('error', this.onError.bind(this));
                    //On message from the server, we parse the commands and send it to the handlers
                    this.socket.on('message', this.onMessage.bind(this));
                    //On player move
                    this.socket.on('move', this.onOtherPlayerMove.bind(this));
                }; //game_core.client_connect_to_server
            }]);