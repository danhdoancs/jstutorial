'use strict';

//Games service used to communicate Games REST endpoints
angular.module('gomokus').factory('GomokuPlayer', ['GomokuConfig', 'HelperService', function (Config, Helper) {
        var Player = function (instance) {

            if (instance) {
                this.state = instance.state;
                this.id = instance.id;
                this.color = instance.color;
                this.firstName = instance.firstName;
                this.name = instance.name;
                this.token = instance.token;
                this.link = instance.link;
                this.picture = instance.picture;
                this.myTurn = instance.myTurn;
                this.isHost = instance.isHost;
                this.isOnline = instance.isOnline;
                this.lastTilePos = instance.lastTilePos;
                this.isWinner = instance.isWinner;
                this.totalWons = instance.totalWons;
                
            } else {
                //Set up initial values for our state information
                this.state = 'not-connected';
                this.id = null;
                this.picture = 'modules/users/img/facebook-default-avatar.png';
                this.myTurn = false;
                this.isHost = false;
                this.isOnline = false;
                this.isWinner = false;
                this.totalWons = 0;

                //These are used in moving us around later
//        this.old_state = {pos:{x:0,y:0}};
                this.lastTilePos = null;
            }
        }; //game_player.constructor

        //On update info
        Player.prototype.updateInfo = function (infos) {
            for (var info in infos) {
                this[info] = infos[info];
            }
        };

        //On connected
        Player.prototype.onConnected = function (data) {
            //The server responded that we are now in a game,
            //this lets us store the information about ourselves and set the colors
            //to show we are now ready to be playing.
//                    GameManager.players.self.id = Facebook.me.id;
            this.state = 'connected';
            this.isOnline = true;
            Helper.log('player id: ' + data.id + ' connected');
        };

        //On join game
        Player.prototype.onJoinGame = function () {
            //We are not the host
            this.isHost = false;
            //Update the local state
            this.state = 'connected.joined.waiting';
            // Guest go second
            this.myTurn = false;
            this.color = Config.PLAYER_GUEST_COLOR;
        };

        //On host game
        Player.prototype.onHostGame = function () {
            //I'm the host
            this.isHost = true;
            //Update state
            this.state = 'hosting.waiting for a player';
            //Host go first
            this.myTurn = true;
            //set host's tile color
            this.color = Config.PLAYER_HOST_COLOR;
        };

        //Handle the move action
        Player.prototype.onMove = function (cellPos) {
            if (!this.myTurn)
                return false;

            //switch to opponent's turn
            this.myTurn = false;

            //record last turn cell
            this.lastTilePos = cellPos;
        };
        
        //Reset status
        Player.prototype.resetStatus = function() {
            this.myTurn = false;
            this.isWinner = false;
            this.isHost = false;
            this.lastTilePos = null;
            this.totalWons = 0;
        }
        
        Player.prototype.serialize = function() {
            return {
                isWinner: this.isWinner,
                isOnline: this.isOnline,
                lastTilePos: this.lastTilePos,
                myTurn: this.myTurn,
                totalWons: this.totalWons,
            };
        };

        return Player;
    }]);