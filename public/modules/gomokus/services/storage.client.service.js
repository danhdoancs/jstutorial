'use strict';

angular.module('gomokus').service('GomokuStorage', ['Facebook', 'gameConfig', 'localStorageService', function (Facebook, gameConfig, localStorage) {
        var id = Facebook.me ? Facebook.me.id : '';
        var localGameStateKey = '6574546783';
        this.gameStateKey = id + localGameStateKey;
        var secretCode = gameConfig.SECRET_PASSPHRASE;
        var flagOldUserKey = 'flagGomokuOldUser';
        var self = this;

        this.storage = localStorage;
        
//        Facebook.addCallback('login', function() {
//            self.setUser(Facebook.me.id);
//        });

// Game state getters/setters and clearing
        this.getGameState = function () {
            var stateJSON = this.storage.get(this.gameStateKey);
            return stateJSON ? JSON.parse(CryptoJS.AES.decrypt(stateJSON, secretCode).toString(CryptoJS.enc.Utf8)) : null;
        };

        this.setGameState = function (gameState) {
            this.storage.set(this.gameStateKey, CryptoJS.AES.encrypt(JSON.stringify(gameState), secretCode));
        };

        this.clearGameState = function () {
            this.storage.remove(this.gameStateKey);
        };

        this.setUser = function (newId) {
            id = newId;
            this.gameStateKey = newId + localGameStateKey;
        };

        this.noUser = function () {
            return id === '' ? true : false;
        };

        this.getFlagOldUser = function () {
            return this.storage.get(flagOldUserKey);
        };
        this.setFlagOldUser = function (flag) {
            this.storage.set(flagOldUserKey, flag);
        };
    }]);
        