'use strict';

// Configuring the Articles module
angular.module('gomokus')
        .config(['localStorageServiceProvider', function (Storage) {
                //Config local storage
                Storage.setPrefix('gomoku')
                        .setStorageType('sessionStorage')
                        .setNotify(true, true);
            }])
        .run(['Menus', 'GomokuCoordinator',
            function (Menus, GomokuCoordinator) {
                // Set top bar menu items
                Menus.addMenuItem('topbar', 'Gomoku', 'gomoku', '', '/gomoku');
                
                //set up socket
                GomokuCoordinator.setup();
            }
        ])
        .constant('GomokuConfig', {
            BOARD_SIZE: 19,
            PLAYER_HOST_COLOR: 0,
            PLAYER_GUEST_COLOR: 1,
            SECRET_PASSPHRASE: 'fuck',
            SECRET_PASSCODE: 'you'
        });