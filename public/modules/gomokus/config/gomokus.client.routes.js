'use strict';

//Setting up route
angular.module('gomokus').config(['$stateProvider',
    function ($stateProvider) {
        // Gomokus state routing
        $stateProvider
                .state('gomoku', {
                    url: '/gomoku/play',
                    templateUrl: 'modules/gomokus/views/gomoku.client.view.html'
                })
                .state('menu', {
                    url: '/gomoku',
                    templateUrl: 'modules/gomokus/views/menu.client.view.html'
                });
    }
]);