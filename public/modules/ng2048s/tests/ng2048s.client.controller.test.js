'use strict';

(function() {
	// Ng2048s Controller Spec
	describe('Ng2048s Controller Tests', function() {
		// Initialize global variables
		var Ng2048sController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Ng2048s controller.
			Ng2048sController = $controller('Ng2048sController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Ng2048 object fetched from XHR', inject(function(Ng2048s) {
			// Create sample Ng2048 using the Ng2048s service
			var sampleNg2048 = new Ng2048s({
				name: 'New Ng2048'
			});

			// Create a sample Ng2048s array that includes the new Ng2048
			var sampleNg2048s = [sampleNg2048];

			// Set GET response
			$httpBackend.expectGET('ng2048s').respond(sampleNg2048s);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.ng2048s).toEqualData(sampleNg2048s);
		}));

		it('$scope.findOne() should create an array with one Ng2048 object fetched from XHR using a ng2048Id URL parameter', inject(function(Ng2048s) {
			// Define a sample Ng2048 object
			var sampleNg2048 = new Ng2048s({
				name: 'New Ng2048'
			});

			// Set the URL parameter
			$stateParams.ng2048Id = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/ng2048s\/([0-9a-fA-F]{24})$/).respond(sampleNg2048);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.ng2048).toEqualData(sampleNg2048);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Ng2048s) {
			// Create a sample Ng2048 object
			var sampleNg2048PostData = new Ng2048s({
				name: 'New Ng2048'
			});

			// Create a sample Ng2048 response
			var sampleNg2048Response = new Ng2048s({
				_id: '525cf20451979dea2c000001',
				name: 'New Ng2048'
			});

			// Fixture mock form input values
			scope.name = 'New Ng2048';

			// Set POST response
			$httpBackend.expectPOST('ng2048s', sampleNg2048PostData).respond(sampleNg2048Response);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Ng2048 was created
			expect($location.path()).toBe('/ng2048s/' + sampleNg2048Response._id);
		}));

		it('$scope.update() should update a valid Ng2048', inject(function(Ng2048s) {
			// Define a sample Ng2048 put data
			var sampleNg2048PutData = new Ng2048s({
				_id: '525cf20451979dea2c000001',
				name: 'New Ng2048'
			});

			// Mock Ng2048 in scope
			scope.ng2048 = sampleNg2048PutData;

			// Set PUT response
			$httpBackend.expectPUT(/ng2048s\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/ng2048s/' + sampleNg2048PutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid ng2048Id and remove the Ng2048 from the scope', inject(function(Ng2048s) {
			// Create new Ng2048 object
			var sampleNg2048 = new Ng2048s({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Ng2048s array and include the Ng2048
			scope.ng2048s = [sampleNg2048];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/ng2048s\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleNg2048);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.ng2048s.length).toBe(0);
		}));
	});
}());