'use strict';

//Setting
var KEY_UP = 'up',
        KEY_DOWN = 'down',
        KEY_RIGHT = 'right',
        KEY_LEFT = 'left';
//
// Configuring the Articles module
angular.module('ng2048s')
        .run(['Menus',
            function (Menus) {
                // Set top bar menu items
                Menus.addMenuItem('topbar', '2048', '2048', '', '/2048');
            }
        ])
        .config(['hammerDefaultOptsProvider', function (hammerDefaultOptsProvider) {
                // Set up Hammer
                hammerDefaultOptsProvider.set({
                    recognizers: [[Hammer.Swipe, {velocity: 0.1, threshold: 1}],
                        [Hammer.Tap, {time: 300}]]
                });
            }])
        .constant('gameConfig', {
            BOARD_SIZE: 4,
            STARTING_TILE_NUMBER: 2,
            SECRET_PASSPHRASE: 'fuck',
            SECRET_PASSCODE: 'you'
        })
        .constant('keyboardConfig', {
            'KEYBOARD_MAP': {
                //arrow keys
                37: KEY_LEFT,
                38: KEY_UP,
                39: KEY_RIGHT,
                40: KEY_DOWN,
                //w,s,d,a
                65: KEY_LEFT,
                68: KEY_RIGHT,
                83: KEY_DOWN,
                87: KEY_UP,
                //Vim
                75: KEY_UP, // Vim up
                76: KEY_RIGHT, // Vim right
                74: KEY_DOWN, // Vim down
                72: KEY_LEFT, // Vim left
            }
        });