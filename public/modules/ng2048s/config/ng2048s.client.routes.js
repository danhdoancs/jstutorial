'use strict';

//Setting up route
angular.module('ng2048s').config(['$stateProvider',
    function ($stateProvider) {
        // Ng2048s state routing
        $stateProvider.
                state('2048', {
                    url: '/2048',
                    templateUrl: 'modules/ng2048s/views/games.client.view.html'
                });
//		state('listNg2048s', {
//			url: '/ng2048s',
//			templateUrl: 'modules/ng2048s/views/list-ng2048s.client.view.html'
//		}).
//		state('createNg2048', {
//			url: '/ng2048s/create',
//			templateUrl: 'modules/ng2048s/views/create-ng2048.client.view.html'
//		}).
//		state('viewNg2048', {
//			url: '/ng2048s/:ng2048Id',
//			templateUrl: 'modules/ng2048s/views/view-ng2048.client.view.html'
//		}).
//		state('editNg2048', {
//			url: '/ng2048s/:ng2048Id/edit',
//			templateUrl: 'modules/ng2048s/views/edit-ng2048.client.view.html'
//		});
    }
]);