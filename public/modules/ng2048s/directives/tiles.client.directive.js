'use strict';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('ng2048s').directive('tile', function(){
    
    return {
        restrict: 'E',
        scope: {
            ngModel: '='
        },
        templateUrl: 'modules/ng2048s/views/tile.client.template.html'
    };
});


