'use strict';
// Games controller
angular.module('ng2048s').controller('GamesController',
        ['GameManager', 'KeyboardService', '$modal', '$timeout', 'Facebook', 'StorageManager', '$rootScope', 'Sounds', '$filter',
            function (GameManager, KeyboardService, $modal, $timeout, Facebook, StorageManager, $rootScope, Sounds, $filter) {
//                var self = this;
                this.game = GameManager;
                this.facebook = Facebook;
                this.soundDisabled = Sounds.isDisabled;

                //set up alerts
//                this.Alerts = Alerts;
//                Alerts.addAlert('success', 'Logged in successfully.');
                //Open a modal window to ask reloading old game
                var modalReloadOldGame = function () {

                    $modal.open({
                        templateUrl: 'modules/ng2048s/views/reload.client.modal.html',
                        controller: 'ReloadController',
                        size: 'md'
                    });
                };

                this.newGame = function () {
                    if (Facebook.loggedIn) {
                        setupUserGame();
                    } else {
                        setupGuestGame();
                    }

                    KeyboardService.init();
                    startGame();

                    // Open guild modal if user is new
                    $timeout(function () {
                        if (!Facebook.loggedIn && !$rootScope.flag2048OldUser && !StorageManager.getFlagOldUser()) {
                            $modal.open({
                                templateUrl: 'modules/ng2048s/views/howtoplay.client.modal.html',
                                controller: 'HowToPlayController',
                                size: 'md'
                            });
                        }
                        //set flag
                        $rootScope.flag2048OldUser = true;
                        StorageManager.setFlagOldUser(true);
                    }, 2000);
                };
                this.restartGame = function () {
                    this.game.restartGame();
                };
                var startGame = function () {

                    KeyboardService.on(function (key) {
                        // Move
                        GameManager.move(key);
//                        console.log('move');
                    });
                };
                this.continue = function () {
                    GameManager.continueGame();
                };

                this.share = function () {
                    Facebook.onSendBrag2048($filter('number')(GameManager.highScore), GameManager.getHighestTile());
                };

                this.shareGame = function () {
                    Facebook.share({
                        caption: 'Game 2048 on CloudyBoard',
                        name: 'CloudyBoard is getting fun! ',
                        picture: 'http://s4.postimg.org/bwkwmpj0t/2048_large.png',
                        link: 'http://192.168.0.105:3000/#!/',
                        description: 'I just got to tile in game 2048 on CloudyBoard, think you can beat me?'
                    }, null);
                };

                //init sounds and toggle 
                this.toggleSound = function () {
                    Sounds.togglePower();
                    Sounds.play('button');
                    //load basic sounds when enabling sounds
                    if (!Sounds.isDisabled()) {
                        $timeout(function () {
                            //init sound after loading 1,5 secs
                            GameManager.initSounds();
                        }, 500);
                    }
                };
                //Open a modal window to read how to play game
                this.modalHowToPlay = function (size) {

                    return $modal.open({
                        templateUrl: 'modules/ng2048s/views/howtoplay.client.modal.html',
                        controller: 'HowToPlayController',
                        size: size
                    });
                };
                //Open a modal window to see leaderboard
                this.modalLeaderboard = function () {
                    return $modal.open({
                        templateUrl: 'modules/ng2048s/views/leaderboard.client.modal.html',
                        controller: 'LeaderboardController',
                        size: 'md'
                    });
                };

                function setupGuestGame() {
                    //Case user logged out
                    //Update storage user
                    StorageManager.setUser('');
                    //Setup game for new user
                    GameManager.setup();
                }

                function setupUserGame() {
                    if (Facebook.loggedIn) {
                        //Setup game for new user
                        GameManager.userGameSetup();
//                      //modal to ask user to load old game
                        //only when there is a local old game
                        if (GameManager.localOldGame) {
//                               console.log('have local old game');
                            if (GameManager.userOldGame) {
                                return modalReloadOldGame();
                            } else {
                                //Transfer old game to user's
                                GameManager.clearUserOldGame();
//                            console.log('clear old game');
                            }
                        } else {
                            if (GameManager.userOldGame)
                                //Reload user old game without asking 
                                GameManager.reloadUserOldGame();
                        }
                    }
                }

                //load new game only when user first time enter page
                if (!$rootScope.playingGames.ng2048) {
                    //handle case when user load other pages and switch to this game
                    //facebook will not change status
                    //user's game will not be loaded
                    this.newGame();
                    this.game.restartGame();
                    $rootScope.playingGames.ng2048 = true;
                }
                //set up callback for facebook status changes
                Facebook.addCallback('login', setupUserGame);
                Facebook.addCallback('logout', setupGuestGame);
            }
        ]);