'use strict';
// Games controller
angular.module('ng2048s').controller('ReloadController', ['$scope', '$modalInstance', 'GameManager', function ($scope, $modalInstance, GameManager) {

        $scope.yes = function () {
            //Reload the old game
            GameManager.reloadUserOldGame();
            //Close modal
            $modalInstance.close();
        };

        $scope.no = function () {
            //Clear the old game state
            GameManager.clearUserOldGame();
            //Use the current game
            $modalInstance.close();
        };
    }]);