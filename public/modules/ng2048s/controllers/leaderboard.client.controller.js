'use strict';
// Games controller
angular.module('ng2048s').controller('LeaderboardController',
        ['$scope', '$modalInstance', 'Leaders', 'Facebook',
            function ($scope, $modalInstance, Leaders, Facebook) {
                var flagLoadFriends;
                $scope.facebook = Facebook;
                $scope.flagTabFriend = false;
                // Get toppers
                $scope.toppers = Leaders.toppers();

                //Get friends
//                $scope.getTopFriends();

                $scope.toggleTabFriend = function (flag) {
                    $scope.flagTabFriend = flag;
                };

                $scope.ok = function () {
                    $modalInstance.close();
                };

                $scope.hasFriendPermission = function () {
                    return Facebook.hasPermission('user_friends');
                };

                // Reask friend list permission, then get top friends records
                $scope.reRequestFriends = function () {
                    Facebook.reRequestFriends(function () {
                        flagLoadFriends = false;
                        $scope.getTopFriends();
                    });
                };

                $scope.getTopFriends = function () {
                    if (!flagLoadFriends) {
                        if (Facebook.friends) {
                            var friendIds = [];
                            for (var i = 0; i < Facebook.friends.length; i++) {
                                friendIds[i] = Facebook.friends[i].id;
                            }
                            friendIds[i] = Facebook.me.id;
                            $scope.friends = Leaders.friends({friends: friendIds});
                            //Show friends that haven't play yet to invite or chalenge
//                for(var i = 0; i < friendIds.length; i++) {
//                    $scope.friends[i] = servFriends[i].user.providerData.id === friendIds[]
//                }
                        }
                    }
                    flagLoadFriends = true;
                };
            }]);