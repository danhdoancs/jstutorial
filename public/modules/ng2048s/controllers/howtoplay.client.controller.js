'use strict';
// Games controller
angular.module('ng2048s').controller('HowToPlayController', ['$scope', '$modalInstance', function ($scope, $modalInstance) {
        $scope.content = {
            'howtoplay': true,
            'newrules': false,
            'tips': false
        };

        $scope.ok = function () {
            $modalInstance.dismiss('cancle');
        };

        $scope.next = function () {
            if ($scope.content.howtoplay) {
                $scope.content.howtoplay = false;
                $scope.content.newrules = true;
            } else if ($scope.content.newrules) {
                $scope.content.newrules = false;
                $scope.content.tips = true;
            }
        };

        $scope.canNext = function () {
            return ($scope.content.howtoplay || $scope.content.newrules);
        };
    }]);