'use strict';

//Games service used to communicate Games REST endpoints
angular.module('ng2048s').service('GameManager',
        ['StorageManager', 'GridService', 'Sounds', 'Ng2048s', 'Facebook', 'gameConfig',
            function (StorageManager, GridService, Sounds, Ng2048s, Facebook, gameConfig) {
                var thisService = this;
                this.grid = GridService.grid;
                this.tiles = GridService.tiles;
                this.gameSize = GridService.getSize();
                this.winningValue = 32;
                this.setNewRecord = false;
                this.highScore = 0;
                this.highTile = 2;
                this.currentScore = 0;
                this.additionalScore = false;
                this.hasCombo = false;
                var Game = null;


                // Load button sound first
                Sounds.load('button', 'modules/ng2048s/sounds/button.mp3');

                this.initSounds = function () {
                    //load basic sound effects
                    Sounds.load('moving', 'modules/ng2048s/sounds/moving.mp3');
                    Sounds.load('merging', 'modules/ng2048s/sounds/merging.mp3');
                    Sounds.load('combo', 'modules/ng2048s/sounds/combo.mp3');
                    Sounds.load('gameover', 'modules/ng2048s/sounds/gameover.mp3');
                    Sounds.load('gamewin', 'modules/ng2048s/sounds/gamewin.mp3');
                };

                this.getHighScore = function () {
//                    return parseInt($cookieStore.get('highScore')) || 0;
                    return StorageManager.getBestScore();
                };

                this.newGame = function () {
                    this.setup();
                };

                this.restartGame = function () {
                    //clear storage
                    StorageManager.clearGameState();
                    this.refresh();
                    //Play sound
                    Sounds.play('button');
                };

                this.refresh = function () {
                    this.gameOver = false;
                    this.win = false;
                    this.setNewRecord = false;
                    this.currentScore = 0;
                    this.keepPlaying = false;

                    //reset up the board
                    GridService.setup();
                };

                this.reloadUserOldGame = function () {
                    if (this.userOldGame) {
                        GridService.initFromState(this.userOldGame.grid.cells); // Reload grid
                        this.currentScore = this.userOldGame.score;
                        this.gameOver = this.userOldGame.over;
                        this.win = this.userOldGame.won;
                        this.keepPlaying = this.userOldGame.keepPlaying;
                        this.setNewRecord = this.userOldGame.setNewRecord;
                    }
                    //Clear the local old game
                    StorageManager.clearLocalData();
                };

                this.clearUserOldGame = function () {
//                    StorageManager.clearGameState();
                    //Transfer local game to user's
                    StorageManager.transferGame();
                    //Clear the local old game
                    StorageManager.clearLocalData();
                };

                //set up when have user
                this.userGameSetup = function () {
                    if (Facebook.me) {
                        //Update storage user
                        StorageManager.setUser(Facebook.me.id);
                        this.userOldGame = StorageManager.getGameState();

                        //Retrieve highScore from server
                        Game = Ng2048s.get(function () {
                            thisService.highScore = (Game && Game.highScore > thisService.getHighScore()) ? Game.highScore : thisService.getHighScore();
                            //Not yet saved when not yet logging in, smaller than the current score
                            if (thisService.highScore < thisService.currentScore)
                            {
                                thisService.setNewRecord = true;
                                thisService.highScore = thisService.currentScore;
                            }
                            else
                                //Sync the setNewRecord flag 
                                //Set it to false when the loaded highScore is higher than the local high score
                                thisService.setNewRecord = false;

                            //Save after logging in
                            // Update current record before get, when people come back from logging in
                            thisService.saveScore();
                        });
                    }
                }

                //load old game or build new game
                this.setup = function () {
                    //load the local game state first if any
                    var previousState = StorageManager.getLocalGameState();

                    // Reload the game from a previous game if present
                    if (previousState) {
                        GridService.initFromState(previousState.grid.cells); // Reload grid
                        this.currentScore = previousState.score;
                        this.gameOver = previousState.over;
                        this.win = previousState.won;
                        this.keepPlaying = previousState.keepPlaying;
                        this.setNewRecord = previousState.setNewRecord;

//                        flag local old game
                        this.localOldGame = true;
                    } else {
                        //reset up the board
                        GridService.setup();
                        this.currentScore = 0;
                        this.gameOver = false;
                        this.win = false;
                        this.keepPlaying = false;
                        this.setNewRecord = false;
                    }

                    // Get high score localStorage
                    this.highScore = thisService.getHighScore();
                    // Clear the userOldgame
                    this.userOldGame = null;
                };

                //Handle the move action
                var hasMoved, hasMerged, additionalScore, originalPosition;
                this.move = function (key) {
                    // Game is over
                    if (this.isGameTerminated()) {
                        return false;
                    }
//                    this.gameOver = true;
//                    this.win = true;
                    hasMoved = false;
                    hasMerged = 0;
                    additionalScore = 0;

                    GridService.allDirections[key].x.forEach(function (x) {
                        GridService.allDirections[key].y.forEach(function (y) {
                            // For every position
                            // Save the tile's original position
                            originalPosition = {x: x, y: y};

                            var tile = GridService.getCellAt(originalPosition);

                            if (tile) {
                                //reset
                                tile.reset();

                                //if we have a tile here
                                var cell = GridService.calculateNextPosition(tile, key),
                                        next = cell.next;

                                //Check if can merge
                                if (next && next.value === tile.value && !next.merged) {
                                    // Handle merge
                                    //Merge the tiles into 1
                                    GridService.mergingState1(tile, next);

                                    //Update the value after moving tile to the new position
                                    GridService.mergingState2(tile);
                                    //Update score
                                    additionalScore += tile.value;
                                    //Check for the winning value
                                    if (!thisService.keepPlaying && tile.value >= thisService.winningValue) {
                                        thisService.win = true;
                                        //Play winning sound
                                        Sounds.play('gamewin');
                                    }
                                    //set flag moved
                                    hasMerged++;

                                    //move only if the new position is different
                                } else if (tile.id !== cell.newPosition.id) {
                                    // Handle moving tile
                                    GridService.moveTile(tile, cell.newPosition);
                                    hasMoved = true;
                                }
                            }
                        });
                    });

                    //Play sound
                    if (!thisService.soundMuted)
                        if (hasMerged) {
                            if (hasMerged === 1)
                                Sounds.play('merging');
                            else
                                Sounds.play('combo');
                        } else if (hasMoved) {
                            Sounds.play('moving');
                        }

                    //Add a random new tile if moved
                    //Update score
                    if (hasMoved || hasMerged) {
                        GridService.randomlyInsertNewTile();
                        //Update the score of the game
                        thisService.updateScore(thisService.currentScore + additionalScore * hasMerged);
                        //Check game over
                        if (!thisService.movesAvailable()) {
                            thisService.gameOver = true;
                            // Clear the state when the game is over (game over only, not win)
                            StorageManager.clearGameState();
                            //Play sound
                            if (thisService.setNewRecord)
                                Sounds.play('gamewin');
                            else
                                Sounds.play('gameover');
                        } else {
                            StorageManager.setGameState(thisService.serialize());
                        }

                        //Save or update score to server
                        thisService.saveScore();
                    }

                    //Set merging times
                    thisService.hasCombo = hasMerged > 1 ? true : false;
                };

                this.saveScore = function () {
                    //Handle win or loose
                    if (this.setNewRecord && (this.gameOver || this.win) && Facebook.loggedIn) {
                        // Save score
                        if (!Game || !Game.highScore) {
                            //Create
                            Game = new Ng2048s({
                                'highScore': CryptoJS.AES.encrypt(this.highScore.toString(), gameConfig.SECRET_PASSCODE).toString(),
                                'fbId': Facebook.me.id
                            });
                            Game.$save();
                        } else {
                            //Update
                            Game.highScore = CryptoJS.AES.encrypt(this.highScore.toString(), gameConfig.SECRET_PASSCODE).toString();
                            Game.$update();
                        }
                    }
                };

                this.getHighestTile = function () {
                    return GridService.getHighestTileValue();
                };

                //Update the score
                this.updateScore = function (newScore) {
                    this.currentScore = newScore;
                    if (this.currentScore > this.highScore) {
                        thisService.updateHighScore(this.currentScore);
                        thisService.setNewRecord = true;
                    }
                };

                this.updateHighScore = function (newHighScore) {
                    this.highScore = newHighScore;
                    StorageManager.setBestScore(newHighScore);
                };

                //Are there moves left?
                this.movesAvailable = function () {
                    return GridService.anyCellsAvailable() ||
                            GridService.tileMatchesAvailable();
                };

                // Return true if the game is lost, or has won and the user hasn't kept playing
                this.isGameTerminated = function () {
                    return this.gameOver || (this.win && !this.keepPlaying);
                };

                // Keep playing after winning (allows going over 2048)
                this.continueGame = function () {
                    this.keepPlaying = true;
                    this.gameOver = false;
                    this.win = false;
                    //Play sound
                    Sounds.play('button');
                };

                // Represent the current game as an object
                this.serialize = function () {
                    return {
                        grid: GridService.serialize(),
                        score: this.currentScore,
                        over: this.gameOver,
                        won: this.win,
                        keepPlaying: this.keepPlaying,
                        setNewRecord: this.setNewRecord
                    };
                };
            }]);