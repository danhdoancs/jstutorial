'use strict';

window.fakeStorage = {
    _data: {},
    setItem: function (id, val) {
        return (this._data[id] = String(val));
    },
    getItem: function (id) {
        return this._data.hasOwnProperty(id) ? this._data[id] : undefined;
    },
    removeItem: function (id) {
        return delete this._data[id];
    },
    clear: function () {
        return (this._data = {});
    }
};

angular.module('ng2048s').service('StorageManager', ['Facebook', 'gameConfig', function (Facebook, gameConfig) {
        var id = Facebook.me ? Facebook.me.id : '';
        this.localBestScoreKey = '7342748241';
        this.localGameStateKey = '8732578342';
        this.bestScoreKey = id + this.localBestScoreKey;
        this.gameStateKey = id + this.localGameStateKey;
        var secretCode = gameConfig.SECRET_PASSPHRASE;
        var flagOldUserKey = 'flag2048OldUser';

        var localStorageSupported = function () {
            var testKey = 'test';
            var storage = window.localStorage;

            try {
                storage.setItem(testKey, '1');
                storage.removeItem(testKey);
                return true;
            } catch (error) {
                return false;
            }
        };
        this.storage = localStorageSupported() ? window.localStorage : window.fakeStorage;

// Best score getters/setters
        this.getBestScore = function () {
            var localScore = this.storage.getItem(this.bestScoreKey);
            return localScore ? CryptoJS.AES.decrypt(localScore, secretCode).toString(CryptoJS.enc.Utf8) : 0;
        };

        this.getLocalBestScore = function () {
            var localScore = this.storage.getItem(this.localBestScoreKey);
            return localScore ? CryptoJS.AES.decrypt(localScore, secretCode).toString(CryptoJS.enc.Utf8) : 0;
        };

        this.setBestScore = function (score) {
            this.storage.setItem(this.bestScoreKey, CryptoJS.AES.encrypt(score.toString(), secretCode));
        };

// Game state getters/setters and clearing
        this.getGameState = function () {
            var stateJSON = this.storage.getItem(this.gameStateKey);
            return stateJSON ? JSON.parse(CryptoJS.AES.decrypt(stateJSON, secretCode).toString(CryptoJS.enc.Utf8)) : null;
        };

        this.getLocalGameState = function () {
            var stateJSON = this.storage.getItem(this.localGameStateKey);
            return stateJSON ? JSON.parse(CryptoJS.AES.decrypt(stateJSON, secretCode).toString(CryptoJS.enc.Utf8)) : null;
        };

        this.setGameState = function (gameState) {
            this.storage.setItem(this.gameStateKey, CryptoJS.AES.encrypt(JSON.stringify(gameState), secretCode));
        };

        this.clearGameState = function () {
            this.storage.removeItem(this.gameStateKey);
        };

        this.clearLocalData = function () {
            this.storage.removeItem(this.localGameStateKey);
            this.storage.removeItem(this.localBestScoreKey);
        };

        this.setUser = function (newId) {
            id = newId;
            this.bestScoreKey = newId + this.localBestScoreKey;
            this.gameStateKey = newId + this.localGameStateKey;
        };

        this.noUser = function () {
            return id === '' ? true : false;
        };

        this.transferGame = function () {
            this.setGameState(this.getLocalGameState());
        };

        this.getFlagOldUser = function () {
            return this.storage.getItem(flagOldUserKey);
        };
        this.setFlagOldUser = function (flag) {
            this.storage.setItem(flagOldUserKey, flag);
        };
    }]);
        