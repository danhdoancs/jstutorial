'use strict';
//Games service used to communicate Games REST endpoints
angular.module('ng2048s').service('Sounds', ['ngAudio', function (ngAudio) {
        var flagLoadSounds = [];
        var flagDisabled = true;
        var sounds = [];

        this.load = function (name, url, flagPlay) {
            //check if already loaded
            if (!flagLoadSounds[name]) {
                //load sound effects
                sounds[name] = ngAudio.load(url);
                sounds[name].unbind();
                //set flag
                flagLoadSounds[name] = true;
            }
            //play sound after loaded
            if (flagPlay)
                this.play(name);
        };

        this.play = function (name) {
            if (!flagDisabled)
                sounds[name].play();
        };
        
        //toggle on/off sounds
        this.togglePower = function() {
            flagDisabled = !flagDisabled;
        };
        
        this.isDisabled = function() {
            return flagDisabled;
        };
    }]);