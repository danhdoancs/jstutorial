'use strict';
//Games service used to communicate Games REST endpoints
angular.module('ng2048s')
        .service('KeyboardService', ['keyboardConfig', '$document', function (keyboardConfig, $document) {
                var thisService = this;
                this.keyEventHandlers = [];
                var flagBind = false;

                this.init = function () {
//                    var gameDiv = angular.element(document.getElementsByClassName('game-pg-2048'));
                    $document.bind('keydown', function (event) {
                        var key = keyboardConfig.KEYBOARD_MAP[event.which];
                        //handle if key is one of arrow keys
                        if (key) {
                            event.preventDefault();
                            thisService._handleKeyEvent(key, event);
                        }
                    });
  
                    //focus on game div
//                    document.getElementsByClassName('game-pg-2048')[0].focus();
                };

                this.unbind = function () {
                        $document.unbind('keydown');
                };

                this.on = function (callback) {
                    this.keyEventHandlers.push(callback);
                };

                this._handleKeyEvent = function (key, event) {
                    var callbacks = this.keyEventHandlers;
                    if (!callbacks) {
                        return;
                    }

                    event.preventDefault();

                    for (var x = 0; x < callbacks.length; x++) {
                        var cb = callbacks[x];
                        cb(key, event);
                    }
                };
            }]);