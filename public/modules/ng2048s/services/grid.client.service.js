'use strict';
//Games service used to communicate Games REST endpoints
angular.module('ng2048s')
        .provider('GridService', ['gameConfig', function (gameConfig) {
                var thisService = this;

                //size of the board
                this.size = gameConfig.BOARD_SIZE;
                this.totalSize = this.size * this.size;
                this.startingTileNumber = gameConfig.STARTING_TILE_NUMBER; // default starting tiles
                this.freeCells = this.totalSize;


//                this.setSize = function (newSize) {
//                    this.size = newSize ? newSize : gameConfig.BOARD_SIZE;
//                    this.totalSize = this.size * this.size;
//                };
//                this.setStartingTiles = function (newTiles) {
//                    this.startingTileNumber = newTiles ? newTiles : gameConfig.STARTING_TILE_NUMBER;
//                };

                this.setup = function () {
                    this.resetFreeCells();
                    this.buildEmptyGameBoard();
                    this.buildStartingPosition();
                };

                this.resetFreeCells = function () {
                    this.freeCells = this.totalSize;
                };

                this.$get = function (TileModel, HelperService) {
                    this.tiles = [];
                    this.grid = [];

                    var vectors = {
                        'left': {x: -1, y: 0},
                        'right': {x: 1, y: 0},
                        'up': {x: 0, y: -1},
                        'down': {x: 0, y: 1}
                    };

                    this.getSize = function () {
                        return this.size;
                    };

                    //load saved game
                    this.initFromState = function (state) {
                        //restore save tiles
                        for (var x = 0; x < this.size; x++) {
                            for (var y = 0; y < this.size; y++) {
                                var pos = y * this.size + x;
                                var tile = state[x][y];
                                //init grid
                                thisService.grid[pos] = null;
                                //restore tile
                                this.tiles[pos] = tile ? this.newTile(tile.position, tile.value) : null;
                            }
                        }
                    };

                    //build game board
                    this.buildEmptyGameBoard = function () {
                        //initialize our grid
                        for (var x = 0; x < thisService.totalSize; x++) {
                            thisService.grid[x] = null;
                        }

                        this.forEach(function (x, y) {
                            thisService.setCellAt({x: x, y: y}, null);
                        });

                        //setup

                    };

                    var traversalDirections = function (key) {
                        var vector = vectors[key];
                        var positions = {x: [], y: []};

                        for (var x = 0; x < thisService.size; x++) {
                            positions.x.push(x);
                            positions.y.push(x);
                        }
                        // Reorder if we are going right
                        if (vector.x > 0) {
                            positions.x = positions.x.reverse();
                        }

                        //Reorder if we are going down
                        if (vector.y > 0) {
                            positions.y = positions.y.reverse();
                        }

                        return positions;
                    };

                    this.allDirections = {
                        'up': traversalDirections('up'),
                        'down': traversalDirections('down'),
                        'right': traversalDirections('right'),
                        'left': traversalDirections('left')
                    };

                    this.calculateNextPosition = function (cell, key) {
                        var vector = vectors[key];
                        var previous;
                        var x = vector.x, y = vector.y;


                        do {
                            previous = cell;
                            cell = {
                                x: previous.x + x,
                                y: previous.y + y
                            };
                        } while (this.cellAvailable(cell));

                        return {
                            newPosition: previous,
                            next: this.getCellAt(cell)
                        };
                    };

                    //is the position within the grid
                    this.withinGrid = function (cell) {
                        return cell.x >= 0 && cell.x < this.size &&
                                cell.y >= 0 && cell.y < this.size;
                    };
                    //is a cell available at a position
                    this.cellAvailable = function (cell) {
                        if (this.withinGrid(cell)) {
                            return !this.getCellAt(cell);
                        } else {
                            return false;
                        }
                    };
                    //build the initial starting position 
                    //with randomly placed tiles
                    this.buildStartingPosition = function () {
                        for (var x = 0; x < this.startingTileNumber; x++) {
                            this.randomlyInsertNewTile();
                        }
                    };
                    //check to see if there is matches available
                    this.tileMatchesAvailable = function () {
                        for (var i = 0; i < this.totalSize; i++) {
                            var pos = HelperService.positionToCoordinates(i, this.size);
                            var tile = this.tiles[i];

                            if (tile) {
                                // Check all vectors
                                for (var vectorName in vectors) {
                                    var vector = vectors[vectorName];
                                    var cell = {x: pos.x + vector.x, y: pos.y + vector.y};
                                    var other = this.getCellAt(cell);
                                    if (other && other.value === tile.value) {
                                        return true;
                                    }
                                }
                            }
                        }
                        return false;
                    };
                    //get a cell at a position
                    this.getCellAt = function (pos) {
                        if (this.withinGrid(pos)) {
                            var x = HelperService.coordinatesToPosition(pos, this.size);
                            return this.tiles[x];
                        } else {
                            return null;
                        }
                    };
                    this.setCellAt = function (pos, tile) {
//                        if (this.withinGrid(pos)) {
                        var xPos = HelperService.coordinatesToPosition(pos, this.size);
                        this.tiles[xPos] = tile;
//                        }
                    };
                    this.moveTile = function (tile, newPosition) {
                        //Update array location
                        this.setCellAt(tile, null);
                        this.setCellAt(newPosition, tile);

                        //Update tile model
                        tile.updatePosition(newPosition);
                    };

                    //run a callback on every cell either on a grid or tile
                    this.forEach = function (cb) {
                        for (var i = 0; i < this.totalSize; i++) {
                            var coordinate = HelperService.positionToCoordinates(i, thisService.size);
                            cb(coordinate.x, coordinate.y, this.tiles[i]);
                        }
                    };
                    //insert a new tile
                    this.insertTile = function (tile) {
//                        var pos = HelperService.coordinatesToPosition(tile, this.size);
                        this.tiles[tile.y * this.size + tile.x] = tile;
                        //count the free cells
                        this.freeCells--;
                    };
                    this.newTile = function (pos, value) {
                        return new TileModel(pos, value);
                    };
                    this.removeTile = function (tile) {
//                        pos = HelperService.coordinatesToPosition(tile, this.size);
                        delete this.tiles[tile.y * this.size + tile.x];
//                        this.tiles[tile.y * this.size + tile.x] = null;
                    };

                    this.mergingState1 = function (tile, next) {
                        //Update array location
                        thisService.setCellAt(tile, null);
                        //Update tile model, moving
                        tile.updatePosition(next);
                        //delete the moving tile
                        thisService.removeTile(next);
                        //arive to the new position
                        thisService.setCellAt(next, tile);
                        //set merged
                        tile.setMerged();
                    };

                    this.mergingState2 = function (tile) {
                        //update the tile
                        tile.updateValue(tile.value * 2);
                        // update the free cells
                        this.freeCells++;
                    };

                    this.samePositions = function (a, b) {
                        return a.x === b.x && a.y === b.y;
                    };

                    //get all available tiles
                    this.availableCells = function () {
                        var cells = [];
                        this.forEach(function (x, y) {
                            var foundTile = thisService.getCellAt({x: x, y: y});
                            if (!foundTile) {
                                cells.push({x: x, y: y});
                            }
                        });
                        return cells;
                    };
                    this.randomlyInsertNewTile = function () {
                        var cell = this.randomAvailableCell();
                        if (!cell) {
                            return false;
                        }
                        var tile = this.newTile(cell, 2);
                        this.insertTile(tile);
                    };
                    //Get a randomly available cell from 
                    //all the currently available cells
                    this.randomAvailableCell = function () {
                        var cells = this.availableCells();
                        if (cells.length > 0) {
                            return cells[Math.floor(Math.random() * cells.length)];
                        }
                    };
                    //check to see there are still available cells
                    this.anyCellsAvailable = function () {
                        return this.freeCells > 0;
                    };

                    //get highest tile
                    this.getHighestTileValue = function () {
                        var highestTile = 2;
                        for (var i = 0; i < this.tiles.length; i++) {
                            var tile = this.tiles[i];
                            if (tile && tile.value > highestTile)
                                highestTile = tile.value;
                        }
                        return highestTile;
                    };

                    this.serialize = function () {
                        var cellState = [];

                        for (var x = 0; x < this.size; x++) {
                            var row = cellState[x] = [];

                            for (var y = 0; y < this.size; y++) {
                                var tile = this.tiles[y * this.size + x];
                                row.push(tile ? tile.serialize() : null);
                            }
                        }

                        return {
                            size: this.size,
                            cells: cellState
                        };
                    };

                    return this;
                };
            }]);