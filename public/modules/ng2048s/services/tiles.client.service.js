'use strict';

//Games service used to communicate Games REST endpoints
angular.module('ng2048s')
        .factory('TileModel', function (GenerateUniqueId) {
            var Tile = function (pos, value) {
                this.x = pos.x;
                this.y = pos.y;
                this.value = value || 2;

                this.id = GenerateUniqueId.next();
                this.merged = null;
                this.new = true;
            };

            Tile.prototype.savePosition = function () {
                this.originalX = this.x;
                this.originalY = this.y;
            };

            Tile.prototype.reset = function () {
                this.merged = null;
                this.new = null;
            };

            Tile.prototype.setMergedBy = function (arr) {
                var thisModel = this;
                arr.forEach(function (tile) {
                    tile.merged = true;
                    tile.updatePosition(thisModel.getPosition());
                });
            };

            Tile.prototype.updateValue = function (newValue) {
                this.value = newValue;
            };

            Tile.prototype.setMerged = function () {
                this.merged = true;
            };

            Tile.prototype.updatePosition = function (newPos) {
                this.x = newPos.x;
                this.y = newPos.y;
            };

            Tile.prototype.getPosition = function () {
                return {
                    x: this.x,
                    y: this.y
                };
            };

            Tile.prototype.serialize = function () {
                return {
                    position: {
                        x: this.x,
                        y: this.y
                    },
                    value: this.value
                };
            };

            return Tile;
        });