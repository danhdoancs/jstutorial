'use strict';

//Ng2048s service used to communicate Ng2048s REST endpoints
angular.module('ng2048s')
        .factory('Ng2048s', ['$resource',
            function ($resource) {
                return $resource('2048', null, {
                    update: {
                        method: 'PUT'
                    }
                });
            }
        ])
        .factory('Leaders', ['$resource',
            function ($resource) {
                return $resource('2048/leaderboard/:category', null, {
                    toppers: {
                        method: 'GET',
                        params: {category: 'toppers'},
                        isArray: true,
                        // Change cache: true when the database is big
                        cache: false
                    },
                    friends: {
                        method: 'POST',
                        params: {category: 'friends'},
                        isArray: true,
                        cache: true
                    }
                });
            }]);