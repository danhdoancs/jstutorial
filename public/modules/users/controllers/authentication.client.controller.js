'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication', '$cookieStore', 'StorageManager',
    function ($scope, $http, $location, Authentication, $cookies, StorageManager) {
        $scope.authentication = Authentication;
        // If user is signed in then redirect back home
        if ($scope.authentication.user) {
            $location.url($cookies.get('oldUrl'));
            //delete cookies
            $cookies.remove('oldUrl');
        }

//        $scope.signup = function () {
//            $http.post('/auth/signup', $scope.credentials).success(function (response) {
//                // If successful we assign the response to the global user model
//                $scope.authentication.user = response;
//
//                if ($scope.oldUrl === '/2048') {
//                    //Update storage user
//                    StorageManager.setUser(response.username);
//                    // Transfer local data to users'
////                    StorageManager.transferGame();
////                    // Clear local data
////                    StorageManager.clearLocalData();
//                }
//
//                // And redirect to the index page
//                $cookies.remove('oldUrl');
//                $location.url($scope.oldUrl);
//            }).error(function (response) {
//                $scope.error = response.message;
//            });
//        };

        $scope.signin = function () {
            $http.post('/auth/login', $scope.credentials).success(function (response) {
                // If successful we assign the response to the global user model
                $scope.authentication.user = response;

                if ($scope.oldUrl === '/2048') {
                    //Update storage user
                    StorageManager.setUser(response.username);
                    // Transfer local data to users'
//                    StorageManager.transferGame();
//                    // Clear local data
//                    StorageManager.clearLocalData();
                }

                // And redirect to the index page
//                console.log('sign in ' + $cookies.get('oldUrl'));
                $location.url($cookies.get('oldUrl'));
                $cookies.remove('oldUrl');
            }).error(function (response) {
                $scope.error = response.message;
            });
        };
    }
]);